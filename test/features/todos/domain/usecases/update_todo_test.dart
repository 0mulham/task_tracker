import 'package:dartz/dartz.dart' as dartz;
import 'package:mockito/mockito.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:tasks_tracker/core/params/update_todo_params.dart';
import 'package:tasks_tracker/features/todos/domain/repositories/todo_repository.dart';
import 'package:tasks_tracker/features/todos/domain/usecases/update_todo.dart';

import '../../../../mock/todo_repository_mock.mocks.dart';

void main() {
  TodoRepository mockTodoRepositroy = MockTodoRepository();

  UpdateTodo usecase = UpdateTodo(mockTodoRepositroy);
  var date = DateTime(DateTime.april);
  setUp(() {});
  test('should update exisitng todo task by repositiroy', () async {
    when(mockTodoRepositroy.updateTodo(
      uid: "uid",
      id: "id",
      title: "title",
      description: 'description',
      dueDate: date,
    )).thenAnswer((_) async {
      return dartz.right(true);
    });

    final actual = await usecase.execute(UpdateTodoParams(
      id: "id",
      uid: "uid",
      description: 'description',
      title: "title",
      dueDate: date,
    ));
    expect(actual, const dartz.Right(true));
    verify(await mockTodoRepositroy.updateTodo(
      id: "id",
      uid: "uid",
      title: "title",
      description: 'description',
      dueDate: date,
    ));
    verifyNoMoreInteractions(mockTodoRepositroy);
  });
}
