import 'package:dartz/dartz.dart' as dartz;
import 'package:mockito/mockito.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:tasks_tracker/core/params/create_todo_params.dart';
import 'package:tasks_tracker/features/todos/domain/entities/todo.dart';
import 'package:tasks_tracker/features/todos/domain/repositories/todo_repository.dart';
import 'package:tasks_tracker/features/todos/domain/usecases/create_todo.dart';

import '../../../../mock/todo_repository_mock.mocks.dart';

void main() {
  TodoRepository mockTodoRepositroy = MockTodoRepository();

  CreateTodo usecase = CreateTodo(mockTodoRepositroy);
  var date = DateTime(DateTime.april);
  setUp(() {});
  final tTask = Todo(
    id: "1",
    title: "title",
    description: 'description',
    isCompleted: true,
    dueDate: date,
    createdAt: date,
    isInProgress: false,
  );
  test('should create new todo task by repositiroy', () async {
    when(mockTodoRepositroy.createTodo(
            uid: "uid",
            id: "id",
            title: "title",
            description: 'description',
            dueDate: date))
        .thenAnswer((_) async {
      return dartz.right(tTask);
    });

    final actual = await usecase.execute(CreateTodoParams(
        uid: "uid",
        id: "id",
        description: 'description',
        title: "title",
        dueDate: date));
    expect(actual, dartz.Right(tTask));
    verify(await mockTodoRepositroy.createTodo(
        uid: "uid",
        id: "id",
        title: "title",
        description: 'description',
        dueDate: date));
    verifyNoMoreInteractions(mockTodoRepositroy);
  });
}
