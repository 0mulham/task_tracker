import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:tasks_tracker/core/params/delete_todo_params.dart';
import 'package:tasks_tracker/features/todos/domain/repositories/todo_repository.dart';
import 'package:tasks_tracker/features/todos/domain/usecases/delete_todo.dart';

import '../../../../mock/todo_repository_mock.mocks.dart';
import 'package:dartz/dartz.dart' as dartz;

void main() {
  TodoRepository mockTodoRepository = MockTodoRepository();
  DeleteTodo usecase = DeleteTodo(mockTodoRepository);

  setUp(() {
    when(mockTodoRepository.deleteTodo(
      id: '0',
      uid: "uid",
    )).thenAnswer((_) async {
      return dartz.right(true);
    });
  });

  test("should delete todo and return bool", () async {
    var result = await usecase.execute(DeleteTodoParams("0", "uid"));
    expect(result, dartz.right(true));
    verify(await mockTodoRepository.deleteTodo(
      id: "0",
      uid: "uid",
    ));
    verifyNoMoreInteractions(mockTodoRepository);
  });
}
