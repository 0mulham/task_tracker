import 'package:dartz/dartz.dart' as dartz;
import 'package:mockito/mockito.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:tasks_tracker/features/todos/domain/entities/todo.dart';
import 'package:tasks_tracker/features/todos/domain/entities/todos_list.dart';
import 'package:tasks_tracker/features/todos/domain/repositories/todo_repository.dart';
import 'package:tasks_tracker/features/todos/domain/usecases/get_todos_list.dart';

import '../../../../mock/todo_repository_mock.mocks.dart';

void main() {
  TodoRepository mockTodoRepositroy = MockTodoRepository();

  GetTodoList usecase = GetTodoList(mockTodoRepositroy);
  var date = DateTime(DateTime.april);
  setUp(() {});
  final tTask = TodosList(values: [
    Todo(
      id: "1",
      createdAt: date,
      dueDate: date,
      description: "test task",
      title: "title task",
      isCompleted: false,
      isInProgress: false,
    )
  ]);
  test('should get todo tasks from repositiroy', () async {
    when(mockTodoRepositroy.getTodosList('uid')).thenAnswer((invocation) {
      var callback = invocation.positionalArguments.single;
      return callback(tTask);
    });

    final actual = usecase.execute('uid');
    expect(actual, dartz.Right(tTask));
    verify(mockTodoRepositroy.getTodosList('uid'));
    verifyNoMoreInteractions(mockTodoRepositroy);
  });
}
