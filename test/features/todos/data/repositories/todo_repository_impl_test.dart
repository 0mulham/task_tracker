import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:tasks_tracker/core/errors/exceptions.dart';
import 'package:tasks_tracker/core/errors/failures.dart';
import 'package:tasks_tracker/features/todos/data/models/todo_list_model.dart';
import 'package:tasks_tracker/features/todos/data/repositories/todo_repository_impl.dart';
import 'package:tasks_tracker/features/todos/domain/entities/todos_list.dart';

import '../../../../mock/network_info_mock.mocks.dart';
import '../../../../mock/todo_local_datasource_mock.mocks.dart';
import '../../../../mock/todo_remote_datasource_mock.mocks.dart';

void main() {
  late MockTodoRemoteDataSource mockTodoRemoteDataSource;
  late MockTodoLocalDataSource mockTodoLocalDataSource;
  late MockNetworkInfo mockNetworkInfo;

  late TodoRepositoryImpl repositoryImpl;

  setUp(() {
    mockTodoRemoteDataSource = MockTodoRemoteDataSource();
    mockTodoLocalDataSource = MockTodoLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repositoryImpl = TodoRepositoryImpl(
        todoRemoteDataSource: mockTodoRemoteDataSource,
        todoLocalDataSource: mockTodoLocalDataSource,
        networkInfo: mockNetworkInfo);
  });

  group('getTodosList', () {
    TodosListModel todoListModel = const TodosListModel(values: []);
    TodosList tTodoListEntity = todoListModel;
    test('should check wether the phone is connected to internet', () async {
      when(mockNetworkInfo.isConnected).thenReturn(true);
      when(mockTodoRemoteDataSource.getTodosList("uid")).thenAnswer((real) {
        var callback = real.positionalArguments.single;

        return callback(todoListModel);
      });
      repositoryImpl.getTodosList("uid");
      verify(mockNetworkInfo.isConnected);
    });

    group('test when device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenReturn(true);
      });
      test(
          'should return remotee data when the call to remote data source is successful',
          () async {
        when(mockTodoRemoteDataSource.getTodosList("uid")).thenAnswer((real) {
          var callback = real.positionalArguments.single;

          return callback(todoListModel);
        });
        var actual = repositoryImpl.getTodosList("uid");
        verify(mockTodoRemoteDataSource.getTodosList("uid")).called(1);
        expect(actual, equals(Right(tTodoListEntity)));
      });

      test(
          'should return ServerFailure data when the call to remote data source is unsuccessful',
          () async {
        when(mockTodoRemoteDataSource.getTodosList("uid"))
            .thenThrow(ServerException());
        var actual = repositoryImpl.getTodosList("uid");
        verify(mockTodoRemoteDataSource.getTodosList("uid")).called(1);
        verifyZeroInteractions(mockTodoLocalDataSource);
        expect(actual, equals(Left(ServerFailure())));
      });
    });

    group('test when device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenReturn(true);
      });
      test('should return local data when there is local data', () async {
        when(mockTodoLocalDataSource.getTodosList()).thenAnswer((_) async {
          return todoListModel;
        });
        var actual = repositoryImpl.getTodosList("uid");
        verifyZeroInteractions(mockTodoRemoteDataSource);
        verify(mockTodoLocalDataSource.getTodosList()).called(1);

        /// compare entity because we're dealing with domain layer
        expect(actual, equals(Right(tTodoListEntity)));
      });

      test(
          'should return CacheFailure when the call to local data source is unsuccessful',
          () async {
        when(mockTodoLocalDataSource.getTodosList())
            .thenThrow(CacheException());
        var actual = repositoryImpl.getTodosList("uid");
        verifyZeroInteractions(mockTodoRemoteDataSource);
        verify(mockTodoLocalDataSource.getTodosList()).called(1);

        /// compare entity because we're dealing with domain layer
        expect(actual, equals(Left(CacheFailure())));
      });
    });
  });
}
