import 'dart:convert';

import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database_mocks/firebase_database_mocks.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:tasks_tracker/core/errors/exceptions.dart';
import 'package:tasks_tracker/features/todos/data/datasources/todos_remote_datasource.dart';
import 'package:tasks_tracker/features/todos/data/models/todo_list_model.dart';

import '../../../../json/json_reader.dart';

void main() {
  late FirebaseDatabase firebaseDatabase;
  late TodoRemoteDataSourceImpl todoRemoteDataSourceImpl;

  // Put fake data
  // const userName = 'Elon musk';
  final jsonData = json.decode(readJson('todo_list.json'));
  final tTodoListModel = TodosListModel.fromJson(jsonData);
  late Map<String, dynamic> fakeData;

  setUp(() {
    fakeData = {"todos": jsonData};
    firebaseDatabase = MockFirebaseDatabase.instance;
    todoRemoteDataSourceImpl =
        TodoRemoteDataSourceImpl(firebaseDatabase: firebaseDatabase);
  });
  test('Should get todos from realtime database ...', () async {
    // var expected = MockFirebaseDatabase.instance.ref('todos').set(fakeData);

    final result = todoRemoteDataSourceImpl.getTodosList("uid");
    expect(await result.first, equals(tTodoListModel.values));
  });
  test(
      'Should throws Server Exception when getting todos from realtime database fails...',
      () async {
    MockFirebaseDatabase.instance.ref('').set(fakeData);

    final call = todoRemoteDataSourceImpl.getTodosList;
    expect(() => call("uid"), throwsA(isInstanceOf<ServerException>()));
  });
}
