import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:tasks_tracker/core/errors/exceptions.dart';
import 'package:tasks_tracker/features/todos/data/datasources/todos_local_datasource.dart';
import 'package:tasks_tracker/features/todos/data/models/todo_list_model.dart';

import 'package:tasks_tracker/core/constants.dart';
import '../../../../json/json_reader.dart';
import '../../../../mock/shared_preferences_mock.mocks.dart';

void main() {
  late TodoLocalDataSourceImpl todoLocalDataSourceImpl;
  late MockSharedPreferences mockSharedPreferences;

  setUp(() {
    mockSharedPreferences = MockSharedPreferences();
    todoLocalDataSourceImpl =
        TodoLocalDataSourceImpl(sharedPreferences: mockSharedPreferences);
  });

  group('getTodosListFromCache', () {
    final jsonData = json.decode(readJson('todo_list.json'));
    final tTodoListModel = TodosListModel.fromJson(jsonData);
    test(
        'should return TodosList Model from SharedPreferences when there is Todos in cache',
        () async {
      when(mockSharedPreferences.getString(any))
          .thenReturn(readJson('todo_list.json'));
      //act
      final result = await todoLocalDataSourceImpl.getTodosList();
      verify(mockSharedPreferences.getString(CACHED_TODOS));
      expect(result.values, equals(tTodoListModel.values));
    });

    test('should return CacheException when there is no Todos in cache',
        () async {
      when(mockSharedPreferences.getString(any)).thenReturn(null);
      //act
      final callGetTodo = todoLocalDataSourceImpl.getTodosList;
      // verifyZeroInteractions(mockSharedPreferences.getString(CACHED_TODOS));
      expect(() => callGetTodo(), throwsA(isInstanceOf<CacheException>()));
    });
  });

  group('addTodoToCache', () {
    const tTodoListModel = TodosListModel(values: []);
    final expectedJson = json.encode(tTodoListModel.toJson());

    test('should add TodosList Model to SharedPreferences', () async {
      when(mockSharedPreferences.setString(CACHED_TODOS, expectedJson))
          .thenAnswer((realInvocation) async => true);
      //act
      todoLocalDataSourceImpl.addTodoToCache(tTodoListModel);

      verify(mockSharedPreferences.setString(CACHED_TODOS, expectedJson));
      // expect(result, equals(tTodoListModel.values));
    });

    test('should add TodosList Model to SharedPreferences fails', () async {
      when(mockSharedPreferences.setString(CACHED_TODOS, expectedJson))
          .thenThrow(CacheException());
      //act
      final call = todoLocalDataSourceImpl.addTodoToCache;

      // verify(mockSharedPreferences.setString(CACHED_TODOS, expectedJson));
      expect(
          () => call(tTodoListModel), throwsA(isInstanceOf<CacheException>()));
    });
  });
}
