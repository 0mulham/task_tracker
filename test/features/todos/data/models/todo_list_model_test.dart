import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:tasks_tracker/features/todos/data/models/todo_list_model.dart';
import 'package:tasks_tracker/features/todos/data/models/todo_model.dart';
import 'package:tasks_tracker/features/todos/domain/entities/todos_list.dart';

import '../../../../json/json_reader.dart';

void main() {
  var date = DateTime.parse("2023-01-26 19:35:54.961");
  final tTodoListModel = TodosListModel(values: [
    TodoModel(
        createdAt: date,
        completedDate: date,
        isCompleted: false,
        startedAt: date,
        isInProgress: false,
        title: "title",
        description: 'description',
        dueDate: date,
        id: '0')
  ]);

  test('should be a subclass of TodoList entity', () {
    expect(tTodoListModel, isA<TodosList>());
  });

  group('fromJson TodoList', () {
    test("should return valid model based on json input", () {
      final List<dynamic> jsonData = json.decode(readJson('todo_list.json'));

      final result = TodosListModel.fromJson(jsonData);

      expect(result.values.first, equals(tTodoListModel.values.first));
    });
  });

  group('toJson', () {
    test("should return valid json based on model object", () {
      final result = tTodoListModel.toJson();
      final jsonMap = {
        "values": [
          {
            "id": "0",
            "title": "title",
            "description": "description",
            "isCompleted": false,
            "isInProgress": false,
            "dueDate": "2023-01-26 19:35:54.961",
            "startedAt": "2023-01-26 19:35:54.961",
            "createdAt": "2023-01-26 19:35:54.961",
            "completedDate": "2023-01-26 19:35:54.961"
          }
        ]
      };
      expect(result, jsonMap);
    });
  });
}
