import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:tasks_tracker/features/todos/data/models/todo_model.dart';
import 'package:tasks_tracker/features/todos/domain/entities/todo.dart';

import '../../../../json/json_reader.dart';

void main() {
  var date = DateTime.parse("2023-01-26 19:35:54.961");
  final tTodoModel = TodoModel(
      createdAt: date,
      completedDate: date,
      isCompleted: false,
      startedAt: date,
      isInProgress: false,
      title: "title",
      description: 'description',
      dueDate: date,
      id: '0');

  test('should be a subclass of todo entity', () {
    expect(tTodoModel, isA<Todo>());
  });

  group('fromJson', () {
    test("should return valid model based on json input", () {
      final Map<String, dynamic> jsonData = json.decode(readJson('todo.json'));

      final result = TodoModel.fromJson(jsonData);

      expect(result, tTodoModel);
    });
  });

  group('toJson', () {
    test("should return valid json based on model object", () {
      final result = tTodoModel.toJson();
      final jsonMap = {
        "id": "0",
        "title": "title",
        "description": "description",
        "isCompleted": false,
        "isInProgress": false,
        "dueDate": "2023-01-26 19:35:54.961",
        "startedAt": "2023-01-26 19:35:54.961",
        "createdAt": "2023-01-26 19:35:54.961",
        "completedDate": "2023-01-26 19:35:54.961"
      };
      expect(result, jsonMap);
    });
  });
}
