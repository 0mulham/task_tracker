import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:dartz/dartz.dart' as dartz;
import 'package:mockito/mockito.dart';
import 'package:tasks_tracker/core/constants.dart';
import 'package:tasks_tracker/core/errors/failures.dart';
import 'package:tasks_tracker/features/todos/data/models/todo_list_model.dart';
import 'package:tasks_tracker/features/todos/domain/entities/todos_list.dart';
import 'package:tasks_tracker/features/todos/presentaion/bloc/todos_bloc.dart';

import '../../../../mock/create_todo_usecase_mock.mocks.dart';
import '../../../../mock/delete_todo_usecase_mock.mocks.dart';
import '../../../../mock/get_todos_usecase_mock.mocks.dart';
import '../../../../mock/reset_todo_usecase_mock.mocks.dart';
import '../../../../mock/sync_todos_mock.mocks.dart';
import '../../../../mock/toggle_status_mock.mocks.dart';
import '../../../../mock/update_todo_usecase_mock.mocks.dart';

class MockStream extends Mock
    implements Stream<dartz.Either<Failure, TodosList>> {}

void main() {
  late TodosBloc todoBloc;
  late MockGetTodoList mockGetTodoList;
  late MockCreateTodo mockCreateTodo;
  late MockResetTodo mockResetTodo;
  late MockDeleteTodo mockDeleteTodo;
  late MockUpdateTodo mockUpdateTodo;
  late MockToggleStatus mockToggleStatus;
  late MockSyncTodos mockSyncTodos;
  setUp(() {
    mockGetTodoList = MockGetTodoList();
    mockCreateTodo = MockCreateTodo();
    mockResetTodo = MockResetTodo();
    mockDeleteTodo = MockDeleteTodo();
    mockUpdateTodo = MockUpdateTodo();
    mockToggleStatus = MockToggleStatus();
    mockSyncTodos = MockSyncTodos();
    todoBloc = TodosBloc(
        updateTodo: mockUpdateTodo,
        getTodoList: mockGetTodoList,
        createTodo: mockCreateTodo,
        deleteTodo: mockDeleteTodo,
        resetTodo: mockResetTodo,
        syncTodos: mockSyncTodos,
        toggleStatus: mockToggleStatus);
  });

  test('InitialState should be empty', () {
    expect(todoBloc.state, equals(Empty()));
  });

  group('getTodosList', () {
    const tTodoList = TodosListModel(values: []);
    Stream<dartz.Either<Failure, TodosList>> streamFunc() async* {
      yield const dartz.Right(tTodoList);
      yield dartz.Left(ServerFailure());
      yield dartz.Left(CacheFailure());
    }

    blocTest(
      'should get data from getTodos usecase',
      build: () {
        when(mockGetTodoList.execute("uid").listen((e) {}))
            .thenAnswer((realInvocation) {
          final data = realInvocation.positionalArguments.single;

          return streamFunc().listen((data));
        });
        return TodosBloc(
            updateTodo: mockUpdateTodo,
            deleteTodo: mockDeleteTodo,
            resetTodo: mockResetTodo,
            getTodoList: mockGetTodoList,
            syncTodos: mockSyncTodos,
            toggleStatus: mockToggleStatus,
            createTodo: mockCreateTodo);
      },
      act: (bloc) => bloc.add(const GetTodoListEvent("uid")),
      expect: () =>
          [Empty(), LoadingTodos(), const LoadedTodos(todosList: tTodoList)],
    );

    blocTest(
      'should get error from getTodos usecase with proper error message when fails',
      build: () {
        when(mockGetTodoList.execute(any)).thenAnswer((realInvocation) {
          var callback = realInvocation.positionalArguments.single;
          return callback(CacheFailure());
        });
        return TodosBloc(
            updateTodo: mockUpdateTodo,
            getTodoList: mockGetTodoList,
            createTodo: mockCreateTodo,
            deleteTodo: mockDeleteTodo,
            resetTodo: mockResetTodo,
            syncTodos: mockSyncTodos,
            toggleStatus: mockToggleStatus);
      },
      act: (bloc) => bloc.add(const GetTodoListEvent("uid")),
      expect: () => [
        Empty(),
        LoadingTodos(),
        const TodoError(errorMessage: GET_TODOS_CACHE_FAILURE_MESSAGE)
      ],
    );

    blocTest(
      'should get error from getTodos usecase with proper error message when fails from server',
      build: () {
        when(mockGetTodoList.execute(any)).thenAnswer((realInvocation) {
          var callback = realInvocation.positionalArguments.single;
          return callback(ServerFailure());
        });
        return TodosBloc(
            updateTodo: mockUpdateTodo,
            getTodoList: mockGetTodoList,
            createTodo: mockCreateTodo,
            deleteTodo: mockDeleteTodo,
            syncTodos: mockSyncTodos,
            toggleStatus: mockToggleStatus,
            resetTodo: mockResetTodo);
      },
      act: (bloc) => bloc.add(const GetTodoListEvent("uid")),
      expect: () => [
        Empty(),
        LoadingTodos(),
        const TodoError(errorMessage: GET_TODOS_SERVER_FAILURE_MESSAGE)
      ],
    );
    // test("should get data from getTodos usecase ", () async {
    //   //arrange
    //   when(mockGetTodoList.execute(any))
    //       .thenAnswer((_) async => dartz.Right(tTodoList)); //act

    //   todoBloc.add(GetTodoListEvent());
    //   //assert
    //   verifyNever(mockGetTodoList.execute(NoParams()));
    // });
  });
}
