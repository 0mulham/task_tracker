import 'package:flutter_test/flutter_test.dart';
import 'package:tasks_tracker/core/themes/app_themes.dart';
import 'package:tasks_tracker/features/settings/data/models/customized_theme_model.dart';
import 'package:tasks_tracker/features/settings/domain/entities/customized_theme.dart';

void main() {
  late CustomizedThemeModel customizedThemeModel;
  AppTheme theme = AppTheme.light;
  setUp(() {
    customizedThemeModel = CustomizedThemeModel(theme: theme);
  });

  test('should be a subclass of CustomizedTheme entity', () {
    expect(customizedThemeModel, isA<CustomizedTheme>());
  });
}
