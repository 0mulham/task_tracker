import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:tasks_tracker/core/errors/exceptions.dart';
import 'package:tasks_tracker/core/themes/app_themes.dart';
import 'package:tasks_tracker/features/settings/data/datasources/customized_theme_datasource.dart';
import 'package:tasks_tracker/features/settings/data/models/customized_theme_model.dart';

import 'package:tasks_tracker/core/constants.dart';
import '../../../../mock/shared_preferences_mock.mocks.dart';

void main() {
  late CustomizedThemeDataSourceImpl customizedThemeDataSourceImpl;
  late MockSharedPreferences mockSharedPreferences;

  setUp(() {
    mockSharedPreferences = MockSharedPreferences();
    customizedThemeDataSourceImpl =
        CustomizedThemeDataSourceImpl(sharedPreferences: mockSharedPreferences);
  });

  group('getCurrentThemeFromCache', () {
    const tCustomizedThemeModel = CustomizedThemeModel(theme: AppTheme.light);
    test(
        'should return AppTheme Model from SharedPreferences when there is Todos in cache',
        () async {
      when(mockSharedPreferences.getString(any)).thenReturn('light');
      //act
      final result = await customizedThemeDataSourceImpl.getCurrentTheme();
      verify(mockSharedPreferences.getString(CACHED_THEME));
      expect(result, equals(tCustomizedThemeModel));
    });

    // test('should return CacheException when something goes wrong', () async {
    //   when(mockSharedPreferences.getString(CACHED_THEME)).thenReturn(null);
    //   //act
    //   final getCurrentTheme = customizedThemeDataSourceImpl.getCurrentTheme;
    //   // verifyZeroInteractions(mockSharedPreferences.getString(CACHED_TODOS));
    //   expect(() => getCurrentTheme(), throwsA(isInstanceOf<CacheException>()));
    // });

    group('addThemeToCache', () {
      const tCustomizedThemeModel = CustomizedThemeModel(theme: AppTheme.light);
      const setTheme = "light";

      test('should add AppTheme to SharedPreferences and return true',
          () async {
        when(mockSharedPreferences.setString(CACHED_THEME, setTheme))
            .thenAnswer((_) async => true);
        //act
        var result = await customizedThemeDataSourceImpl
            .setCurrentTheme(tCustomizedThemeModel);

        // verify(mockSharedPreferences.setString(CACHED_TODOS, setTheme));
        expect(result, equals(true));
      });

      test('should throw CacheExcepetion when theme is not set', () async {
        when(mockSharedPreferences.setString(any, any))
            .thenThrow(CacheException());
        //act
        var call = customizedThemeDataSourceImpl.setCurrentTheme;

        // verify(mockSharedPreferences.setString(CACHED_TODOS, setTheme));
        expect(() => call(tCustomizedThemeModel),
            throwsA(isInstanceOf<CacheException>()));
      });

      // test('should add TodosList Model to SharedPreferences fails', () async {
      //   when(mockSharedPreferences.setString(CACHED_TODOS, expectedJson))
      //       .thenThrow(CacheException());
      //   //act
      //   final call = todoLocalDataSourceImpl.addTodoToCache;

      //   // verify(mockSharedPreferences.setString(CACHED_TODOS, expectedJson));
      //   expect(() => call(tTodoListModel),
      //       throwsA(isInstanceOf<CacheException>()));
      // });
    });
  });
}
