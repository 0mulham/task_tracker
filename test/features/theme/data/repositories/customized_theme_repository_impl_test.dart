import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:tasks_tracker/core/errors/exceptions.dart';
import 'package:tasks_tracker/core/errors/failures.dart';
import 'package:tasks_tracker/core/themes/app_themes.dart';
import 'package:tasks_tracker/features/settings/data/models/customized_theme_model.dart';
import 'package:tasks_tracker/features/settings/data/repositories/customized_theme_repository_impl.dart';
import 'package:tasks_tracker/features/settings/domain/entities/customized_theme.dart';

import '../../../../mock/customized_theme_data_source_mock.mocks.dart';

void main() {
  late MockCustomizedThemeDataSource mockCustomizedThemeDataSource;

  late CustomizedThemeRepositoryImpl repositoryImpl;

  setUp(() {
    mockCustomizedThemeDataSource = MockCustomizedThemeDataSource();
    repositoryImpl = CustomizedThemeRepositoryImpl(
      customizedThemeDataSource: mockCustomizedThemeDataSource,
    );
  });

  group('getCurrentTheme', () {
    CustomizedThemeModel customizedThemeModel =
        const CustomizedThemeModel(theme: AppTheme.light);
    CustomizedTheme tCustomizedTheme = customizedThemeModel;

    test('should return theme data when there is local data', () async {
      when(mockCustomizedThemeDataSource.getCurrentTheme())
          .thenAnswer((_) async {
        return customizedThemeModel;
      });
      var actual = await repositoryImpl.getCurrentTheme();
      verify(mockCustomizedThemeDataSource.getCurrentTheme()).called(1);
      // verifyZeroInteractions(mockCustomizedThemeDataSource);

      /// compare entity because we're dealing with domain layer
      expect(actual, equals(Right(tCustomizedTheme)));
    });

    test(
        'should return CacheThemeFailure when the call to  data source is unsuccessful',
        () async {
      when(mockCustomizedThemeDataSource.getCurrentTheme())
          .thenThrow(CacheException());
      var actual = await repositoryImpl.getCurrentTheme();

      verify(mockCustomizedThemeDataSource.getCurrentTheme());
      // .called(1);

      /// compare entity because we're dealing with domain layer
      expect(actual, equals(Left(CacheThemeFailure())));
    });
  });

  group('changeThemeData', () {
    CustomizedThemeModel customizedThemeModel =
        const CustomizedThemeModel(theme: AppTheme.light);
    CustomizedTheme tCustomizedTheme = customizedThemeModel;

    test('should change theme data  ', () async {
      when(mockCustomizedThemeDataSource.setCurrentTheme(customizedThemeModel))
          .thenAnswer((_) async {
        return true;
      });
      repositoryImpl.changeTheme(tCustomizedTheme);
      verify(
          mockCustomizedThemeDataSource.setCurrentTheme(customizedThemeModel));

      /// compare entity because we're dealing with domain layer
      // expect(actual, equals(Right(tCustomizedTheme)));
    });
  });
}
