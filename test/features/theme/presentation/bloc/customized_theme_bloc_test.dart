import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:dartz/dartz.dart' as dartz;
import 'package:mockito/mockito.dart';
import 'package:tasks_tracker/core/themes/app_themes.dart';
import 'package:tasks_tracker/features/settings/data/models/customized_theme_model.dart';
import 'package:tasks_tracker/features/settings/domain/entities/customized_theme.dart';
import 'package:tasks_tracker/features/settings/presentaion/bloc/customized_theme_bloc.dart';

import '../../../../mock/chane_theme_usecase_mock.mocks.dart';
import '../../../../mock/get_theme_usecase_mock.mocks.dart';

void main() {
  late CustomizedThemeBloc customizedThemeBloc;
  late MockGetTheme mockGetTheme;
  late MockChangeTheme mockChangeTheme;

  setUp(() {
    mockGetTheme = MockGetTheme();
    mockChangeTheme = MockChangeTheme();
    customizedThemeBloc = CustomizedThemeBloc(
        changeTheme: mockChangeTheme, getTheme: mockGetTheme);
  });

  test('InitialState should be empty', () {
    expect(
        customizedThemeBloc.state,
        equals(const CustomizedThemeInitial(
            customizedTheme: CustomizedTheme(theme: AppTheme.light))));
  });

  group('getTheme', () {
    const tCustomizedTheme = CustomizedThemeModel(theme: AppTheme.light);
    const tCustomizedThemeEntity = CustomizedTheme(theme: AppTheme.light);

    blocTest(
      'should get data from them usecase',
      build: () {
        when(mockGetTheme.execute(any))
            .thenAnswer((_) async => dartz.right(tCustomizedTheme));
        return CustomizedThemeBloc(
            getTheme: mockGetTheme, changeTheme: mockChangeTheme);
      },
      act: (bloc) => bloc.add(GetAppThemeEvent()),
      expect: () => [
        const CustomizedThemeInitial(customizedTheme: tCustomizedThemeEntity),
        const CustomizedThemeLoaded(customizedTheme: tCustomizedTheme)
      ],
    );
    var appTheme = AppTheme.light;
    blocTest(
      'should set app theme usecase',
      build: () {
        when(mockChangeTheme.execute(any));
        return CustomizedThemeBloc(
            getTheme: mockGetTheme, changeTheme: mockChangeTheme);
      },
      act: (bloc) => bloc.add(SetAppThemeEvent(appTheme: appTheme)),
      expect: () =>
          [const CustomizedThemeLoaded(customizedTheme: tCustomizedTheme)],
    );

    // blocTest(
    //   'should get error from getTodos usecase with proper error message when fails',
    //   build: () {
    //     when(mockGetTodoList.execute(any))
    //         .thenAnswer((realInvocation) async => dartz.left(CacheFailure()));
    //     return TodosBloc(
    //         getTodoList: mockGetTodoList, createTodo: mockCreateTodo);
    //   },
    //   act: (bloc) => bloc.add(GetTodoListEvent()),
    //   expect: () => [
    //     Empty(),
    //     LoadingTodos(),
    //     const TodoError(errorMessage: GET_TODOS_CACHE_FAILURE_MESSAGE)
    //   ],
    // );

    // blocTest(
    //   'should get error from getTodos usecase with proper error message when fails from server',
    //   build: () {
    //     when(mockGetTodoList.execute(any))
    //         .thenAnswer((realInvocation) async => dartz.left(ServerFailure()));
    //     return TodosBloc(
    //         getTodoList: mockGetTodoList, createTodo: mockCreateTodo);
    //   },
    //   act: (bloc) => bloc.add(GetTodoListEvent()),
    //   expect: () => [
    //     Empty(),
    //     LoadingTodos(),
    //     const TodoError(errorMessage: GET_TODOS_SERVER_FAILURE_MESSAGE)
    //   ],
    // );
    // test("should get data from getTodos usecase ", () async {
    //   //arrange
    //   when(mockGetTodoList.execute(any))
    //       .thenAnswer((_) async => dartz.Right(tTodoList)); //act

    //   todoBloc.add(GetTodoListEvent());
    //   //assert
    //   verifyNever(mockGetTodoList.execute(NoParams()));
    // });
  });
}
