import 'package:mockito/mockito.dart';

import 'package:flutter_test/flutter_test.dart';

import '../../../../mock/customize_theme_repo_mock.mocks.dart';

void main() {
  late MockCustomizedThemeRepository mockCustomizedThemeRepo;

  setUp(() {
    mockCustomizedThemeRepo = MockCustomizedThemeRepository();
  });

  test('should change theme by repositiroy', () async {
    when(mockCustomizedThemeRepo.changeTheme(any));

    // expect(actual, dartz.Right(tTask));
    verify(mockCustomizedThemeRepo.changeTheme(any));
    verifyNoMoreInteractions(mockCustomizedThemeRepo);
  });
}
