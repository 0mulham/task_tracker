import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:tasks_tracker/core/params/no_params.dart';
import 'package:tasks_tracker/core/themes/app_themes.dart';
import 'package:tasks_tracker/features/settings/domain/entities/customized_theme.dart';
import 'package:tasks_tracker/features/settings/domain/usecases/get_theme_usecase.dart';

import '../../../../mock/customize_theme_repo_mock.mocks.dart';

void main() {
  late MockCustomizedThemeRepository mockCustomizedThemeRepo;

  late GetTheme usecase;
  setUp(() {
    mockCustomizedThemeRepo = MockCustomizedThemeRepository();
    usecase = GetTheme(mockCustomizedThemeRepo);
  });

  test('should get theme from repository', () async {
    const theme = CustomizedTheme(theme: AppTheme.light);
    when(mockCustomizedThemeRepo.getCurrentTheme())
        .thenAnswer((_) async => const Right(theme));

    final actual = await usecase.execute(NoParams());
    expect(actual, const Right(theme));
    verify(mockCustomizedThemeRepo.getCurrentTheme());
    verifyNoMoreInteractions(mockCustomizedThemeRepo);
  });
}
