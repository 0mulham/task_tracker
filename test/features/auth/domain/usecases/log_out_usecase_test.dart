import 'package:dartz/dartz.dart' as dartz;
import 'package:mockito/mockito.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:tasks_tracker/core/params/no_params.dart';
import 'package:tasks_tracker/features/auth/domain/usecases/log_out_usecase.dart';

import '../../../../mock/user_respoistory_mock.mocks.dart';

void main() {
  late MockUserRepository mockUserRepository;

  late Logout logout;
  setUp(() {
    mockUserRepository = MockUserRepository();
    logout = Logout(mockUserRepository);
  });
  Future<void> val = Future.value();
  test('should log user out  by repositiroy', () async {
    when(mockUserRepository.logOutUser()).thenAnswer((_) async {
      return dartz.right(val);
    });

    final actual = await logout(NoParams());
    // .execute(CreateTodoParams(description: 'description', title: "title"));
    expect(actual, dartz.Right(val));
    verify(await mockUserRepository.logOutUser());
    verifyNoMoreInteractions(mockUserRepository);
  });
}
