import 'package:dartz/dartz.dart' as dartz;
import 'package:firebase_auth_mocks/firebase_auth_mocks.dart';
import 'package:mockito/mockito.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:tasks_tracker/core/params/no_params.dart';
import 'package:tasks_tracker/features/auth/domain/entities/user.dart';
import 'package:tasks_tracker/features/auth/domain/usecases/get_signed_user_usecase.dart';

import '../../../../mock/user_respoistory_mock.mocks.dart';

void main() {
  late MockUserRepository mockUserRepository;
  late GetSignedUser getSigned;
  late MockUser mockedUser;
  setUp(() {
    mockUserRepository = MockUserRepository();
    getSigned = GetSignedUser(mockUserRepository);
    mockedUser = MockUser();
  });
  test('should get user data by repositiroy', () async {
    UserData val = UserData(userCredential: mockedUser);

    when(mockUserRepository.getSignedUser()).thenAnswer((_) async {
      return dartz.right(val);
    });

    final actual = await getSigned(NoParams());
    // .execute(CreateTodoParams(description: 'description', title: "title"));
    expect(actual, equals((dartz.right(val))));
    verify(await mockUserRepository.getSignedUser());
    verifyNoMoreInteractions(mockUserRepository);
  });
}
