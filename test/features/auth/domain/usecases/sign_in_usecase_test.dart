import 'package:dartz/dartz.dart' as dartz;
import 'package:firebase_auth_mocks/firebase_auth_mocks.dart';
import 'package:mockito/mockito.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:tasks_tracker/core/params/no_params.dart';
import 'package:tasks_tracker/features/auth/domain/entities/user.dart';
import 'package:tasks_tracker/features/auth/domain/usecases/sign_in_usecase.dart';

import '../../../../mock/user_respoistory_mock.mocks.dart';

void main() {
  late MockUserRepository mockUserRepository;

  late SignIn signIn;
  setUp(() {
    mockUserRepository = MockUserRepository();
    signIn = SignIn(mockUserRepository);
  });
  test('should sing user in by repositiroy', () async {
    UserData val = UserData(userCredential: MockUser());

    when(mockUserRepository.signInUser()).thenAnswer((_) async {
      return dartz.right(val);
    });

    final actual = await signIn(NoParams());
    // .execute(CreateTodoParams(description: 'description', title: "title"));
    expect(actual, dartz.Right(val));
    verify(await mockUserRepository.signInUser());
    verifyNoMoreInteractions(mockUserRepository);
  });
}
