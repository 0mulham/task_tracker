// import 'package:dartz/dartz.dart' as dartz;
// import 'package:mockito/mockito.dart';

// import 'package:flutter_test/flutter_test.dart';
// import 'package:tasks_tracker/core/params/create_todo_params.dart';
// import 'package:tasks_tracker/core/usecases/usecase.dart';
// import 'package:tasks_tracker/features/auth/domain/usecases/check_is_signin_usecase.dart';
// import 'package:tasks_tracker/features/auth/domain/usecases/log_out_usecase.dart';
// import 'package:tasks_tracker/features/auth/domain/usecases/sign_in_usecase.dart';
// import 'package:tasks_tracker/features/todos/domain/entities/todo.dart';
// import 'package:tasks_tracker/features/todos/domain/repositories/todo_repository.dart';
// import 'package:tasks_tracker/features/todos/domain/usecases/create_todo.dart';

// import '../../../../mock/todo_repository_mock.mocks.dart';
// import '../../../../mock/user_respoistory_mock.mocks.dart';

// void main() {
//   late MockUserRepository mockUserRepository;

//   late IsSignedIn isSignedIn;
//   setUp(() {
//     mockUserRepository = MockUserRepository();
//     isSignedIn = IsSignedIn(mockUserRepository);
//   });

//   test('should check that user is signed In repositiroy', () async {
//     when(mockUserRepository.isSignedIn()).thenAnswer((_) async {
//       return dartz.right(true);
//     });

//     final actual = await isSignedIn(NoParams());
//     // .execute(CreateTodoParams(description: 'description', title: "title"));
//     expect(actual, dartz.Right(true));
//     verify(await mockUserRepository.isSignedIn());
//     verifyNoMoreInteractions(mockUserRepository);
//   });

//   test('should check that user is not signed In repositiroy', () async {
//     when(mockUserRepository.isSignedIn()).thenAnswer((_) async {
//       return dartz.right(false);
//     });

//     final actual = await isSignedIn(NoParams());
//     // .execute(CreateTodoParams(description: 'description', title: "title"));
//     expect(actual, dartz.Right(false));
//     verify(await mockUserRepository.isSignedIn());
//     verifyNoMoreInteractions(mockUserRepository);
//   });
// }
