import 'package:firebase_auth_mocks/firebase_auth_mocks.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:tasks_tracker/features/auth/data/models/user_model.dart';
import 'package:tasks_tracker/features/auth/domain/entities/user.dart';

void main() {
  test('should be a subclass of UserData entity', () {
    final tTodoListModel = UserDataModel(userCredential: MockUser());

    expect(tTodoListModel, isA<UserData>());
  });
}
