// import 'package:firebase_auth_mocks/firebase_auth_mocks.dart';
// import 'package:flutter_test/flutter_test.dart';
// import 'package:mockito/mockito.dart';
// import 'package:tasks_tracker/features/auth/data/datasources/auth_datasource.dart';

// import '../../../../mock/user_credential_mock.mocks.dart';

// void main() {
//   late AuthDataSourceImpl authDataSourceImpl;
//   late MockFirebaseAuth mockFirebaseAuth;

//   setUp(() {
//     mockFirebaseAuth = MockFirebaseAuth();

//     authDataSourceImpl = AuthDataSourceImpl(mockFirebaseAuth);
//   });

//   group('signIn', () {
//     final user = MockUserCredential();
//     test('should sign in anonymously using firebase auth', () async {
//       var result = await authDataSourceImpl.signIn();
//       var matcher = await (mockFirebaseAuth.signInAnonymously());
//       expect(result.userCredential, equals(matcher.user));
//     });
//   });
// }
