import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:tasks_tracker/core/network/network_info.dart';

import '../../mock/data_connection_checker_mock.mocks.dart';

void main() {
  late NetworkInfoImpl networkInfoImpl;
  late MockInternetConnectionChecker mockInternetConnectionChecker;

  setUp(() {
    mockInternetConnectionChecker = MockInternetConnectionChecker();
    networkInfoImpl = NetworkInfoImpl(mockInternetConnectionChecker);
  });

  group('is connected to internet', () {
    final tHasConnectionFuture = Future.value(true);
    test("should forward the call to InternetConnectionChecker.hasConnection",
        () async {
      when(mockInternetConnectionChecker.hasConnection).thenAnswer((_) {
        ///could be true or false
        return tHasConnectionFuture;
      });

      final result = networkInfoImpl.isConnected;

      verify(mockInternetConnectionChecker.hasConnection);
      expect(result, tHasConnectionFuture);
    });
  });
}
