// Mocks generated by Mockito 5.3.2 from annotations
// in tasks_tracker/test/mock/customized_theme_data_source_mock.dart.
// Do not manually edit this file.

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'dart:async' as _i4;

import 'package:mockito/mockito.dart' as _i1;
import 'package:tasks_tracker/features/settings/data/datasources/customized_theme_datasource.dart'
    as _i3;
import 'package:tasks_tracker/features/settings/data/models/customized_theme_model.dart'
    as _i2;
import 'package:tasks_tracker/features/settings/domain/entities/customized_theme.dart'
    as _i5;

// ignore_for_file: type=lint
// ignore_for_file: avoid_redundant_argument_values
// ignore_for_file: avoid_setters_without_getters
// ignore_for_file: comment_references
// ignore_for_file: implementation_imports
// ignore_for_file: invalid_use_of_visible_for_testing_member
// ignore_for_file: prefer_const_constructors
// ignore_for_file: unnecessary_parenthesis
// ignore_for_file: camel_case_types
// ignore_for_file: subtype_of_sealed_class

class _FakeCustomizedThemeModel_0 extends _i1.SmartFake
    implements _i2.CustomizedThemeModel {
  _FakeCustomizedThemeModel_0(
    Object parent,
    Invocation parentInvocation,
  ) : super(
          parent,
          parentInvocation,
        );
}

/// A class which mocks [CustomizedThemeDataSource].
///
/// See the documentation for Mockito's code generation for more information.
class MockCustomizedThemeDataSource extends _i1.Mock
    implements _i3.CustomizedThemeDataSource {
  MockCustomizedThemeDataSource() {
    _i1.throwOnMissingStub(this);
  }

  @override
  _i4.Future<_i2.CustomizedThemeModel> getCurrentTheme() => (super.noSuchMethod(
        Invocation.method(
          #getCurrentTheme,
          [],
        ),
        returnValue: _i4.Future<_i2.CustomizedThemeModel>.value(
            _FakeCustomizedThemeModel_0(
          this,
          Invocation.method(
            #getCurrentTheme,
            [],
          ),
        )),
      ) as _i4.Future<_i2.CustomizedThemeModel>);
  @override
  _i4.Future<bool> setCurrentTheme(_i5.CustomizedTheme? appTheme) =>
      (super.noSuchMethod(
        Invocation.method(
          #setCurrentTheme,
          [appTheme],
        ),
        returnValue: _i4.Future<bool>.value(false),
      ) as _i4.Future<bool>);
}
