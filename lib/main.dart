import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tasks_tracker/core/localization/app_localizations_setup.dart';
import 'package:tasks_tracker/core/localization/bloc/locale_bloc.dart';

import 'core/localization/app_localizations.dart';
import 'core/themes/app_themes.dart';
import 'features/auth/presentation/bloc/auth_bloc_bloc.dart';
import 'features/auth/presentation/pages/login_page.dart';
import 'features/settings/presentaion/bloc/customized_theme_bloc.dart';
import 'features/todos/presentaion/bloc/todos_bloc.dart';
import 'firebase_options.dart';
import 'injection_container.dart' as dependency_injection;
import 'injection_container.dart';
import 'pages/home_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    name: "task_tracker",
    options: DefaultFirebaseOptions.currentPlatform,
  );

  await dependency_injection.init();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<CustomizedThemeBloc>(
            create: (context) =>
                dependency_injection.serviceLocator<CustomizedThemeBloc>()
                  ..add(GetAppThemeEvent()),
          ),
          BlocProvider<AuthBlocBloc>(
            create: (context) =>
                dependency_injection.serviceLocator<AuthBlocBloc>()
                  ..add(GetUserData()),
          ),
          BlocProvider<LocaleBloc>(
              create: (context) =>
                  dependency_injection.serviceLocator<LocaleBloc>()
                    ..add(ToEnglish())),
        ],
        child: BlocBuilder<CustomizedThemeBloc, CustomizedThemeState>(
            builder: _buildWithTheme));
  }

  Widget _buildWithTheme(context, CustomizedThemeState themState) {
    return BlocBuilder<AuthBlocBloc, AuthBlocState>(builder: (context, state) {
      return BlocProvider<TodosBloc>(
          create: (context) => serviceLocator<TodosBloc>()
            ..add(GetTodoListEvent(
                state is Logged ? state.userData.userCredential.uid : "")),
          child: BlocBuilder<LocaleBloc, LocaleState>(
            buildWhen: (previous, current) => previous != current,
            builder: (context, localeState) {
              return MaterialApp(
                  localizationsDelegates:
                      AppLocalizationsSetup.localizationsDelegates,
                  supportedLocales: AppLocalizationsSetup.supportedLocales,
                  localeResolutionCallback: (locale, b) =>
                      AppLocalizationsSetup.localeResolutionCallback(
                          locale!, b),
                  locale: localeState is HasLocale
                      ? localeState.locale
                      : Locale("en"),
                  title: 'Task Tracker',
                  debugShowCheckedModeBanner: false,
                  theme: appThemeData[themState.customizedTheme.theme],
                  home:
                      (state is Logged) ? const HomePage() : const LoginPage());
            },
          ));
    });
  }
}
