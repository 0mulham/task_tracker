/// an interface for usecases
abstract class ThemeUsecase<Type, Params> {
  Type execute(Params params);
}
