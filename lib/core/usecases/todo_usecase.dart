import 'package:dartz/dartz.dart' as dartz;

import '../errors/failures.dart';

/// an interface for usecases
abstract class TodoUseCase<Type, Params> {
  Future<dartz.Either<Failure, Type>> execute(Params params);
}

abstract class GetTodoUseCase<Type, Params> {
  Stream<dartz.Either<Failure, Type>> execute(Params params);
}

abstract class ToggleStatusUseCase<Type, Params> {
  Future<void> execute(Params params);
}
