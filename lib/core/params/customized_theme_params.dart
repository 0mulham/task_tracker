import '../../features/settings/domain/entities/customized_theme.dart';

class CustomizedThemeParams {
  final CustomizedTheme customizedTheme;
  CustomizedThemeParams(this.customizedTheme);
}
