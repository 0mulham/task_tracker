import '../../features/todos/domain/entities/todo.dart';

class ToggleStatusParams {
  final String id;
  final TodoStatus status;
  final String uid;

  ToggleStatusParams(
      {required this.id, required this.uid, required this.status});
}
