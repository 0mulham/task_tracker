class DeleteTodoParams {
  final String id;
  final String uid;
  DeleteTodoParams(this.id, this.uid);
}
