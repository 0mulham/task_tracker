class CreateTodoParams {
  final String id;

  final String title;
  final String description;
  final DateTime dueDate;
  final String uid;

  CreateTodoParams(
      {required this.id,
      required this.uid,
      required this.description,
      required this.title,
      required this.dueDate});
}
