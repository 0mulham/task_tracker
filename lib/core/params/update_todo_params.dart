class UpdateTodoParams {
  final String title;
  final String description;
  final String id;
  final DateTime dueDate;
  final String uid;

  UpdateTodoParams(
      {required this.description,
      required this.id,
      required this.uid,
      required this.title,
      required this.dueDate});
}
