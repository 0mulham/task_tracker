/// local storage key for todos
// ignore_for_file: constant_identifier_names

const String CACHED_TODOS = "CACHED_TODOS";
const String CACHED_THEME = "CACHED_THEME";
const String CACHED_USERID = "CACHED_USERID";

const String TODO_CREATED_MESSAGE = "Todo created successfully";
const String TODO_DELETED_MESSAGE = "Todo deleted successfully";
const String TODO_UPDATED_MESSAGE = "Todo updated successfully";
const String TODO_STATUS_UPDATED = "Status updated successfully";

const NO_INTERNET_CONNECTION_ERROR_MESSAGE =
    "Please make sure that you have an active intenret connection";
const LOGIN_ERROR_MESSAGE = "Something went wrong";
const GET_TODOS_SERVER_FAILURE_MESSAGE =
    "Unable to get data from server. please make sure that you have an internet connection";
const GET_TODOS_CACHE_FAILURE_MESSAGE = "Unable to get data from your device.";
const UNEXPECTED_ERROR = "Unexpected error happend";
const DELETE_TODO_FAILURE_MESSAGE =
    "We are not able to delete this at the moment.";
const UPDATE_TODO_FAILURE_MESSAGE =
    "We are not able to update this at the moment";
