import 'package:internet_connection_checker/internet_connection_checker.dart';
import '../../features/auth/presentation/bloc/auth_bloc_bloc.dart';
import '../../injection_container.dart';

import '../../features/todos/presentaion/bloc/todos_bloc.dart';

abstract class NetworkInfo {
  bool get isConnected;
}

class NetworkInfoImpl implements NetworkInfo {
  final InternetConnectionChecker internetConnectionChecker;
  bool _isConnected = false;

  NetworkInfoImpl(this.internetConnectionChecker) {
    internetConnectionChecker.onStatusChange.listen((e) {
      if (e == InternetConnectionStatus.connected) {
        _isConnected = true;
        var authState = serviceLocator<AuthBlocBloc>().state;
        if (authState is Logged) {
          serviceLocator<TodosBloc>().add(SyncOfflineWithOnlineTodos(
              authState.userData.userCredential.uid));
        }
      } else {
        _isConnected = false;
      }
    });
  }

  @override
  bool get isConnected => _isConnected;
}
