import 'package:intl/intl.dart' as intl;

class DateFormatter {
  static format(DateTime date) {
    final intl.DateFormat formatter = intl.DateFormat('yyyy-MM-dd HH:MM');
    final String formatted = formatter.format(date);
    return formatted;
  }

  static String getDuration(DateTime from, DateTime to) {
    from = DateTime(from.year, from.month, from.day, from.hour, from.minute);
    to = DateTime(
      to.year,
      to.month,
      to.day,
      to.hour,
      to.minute,
    );
    return "${(to.difference(from).inMinutes).round()} minute";
  }
}
