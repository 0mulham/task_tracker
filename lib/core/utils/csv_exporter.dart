import '../../features/todos/domain/entities/todos_list.dart';
// ignore: library_prefixes
import 'package:to_csv/to_csv.dart' as exportCSV;

import '../../features/todos/domain/entities/todo.dart';

class CsvExporterService {
  static void exportToCsv(TodosList todosListModel) {
    List<String> header = [
      'id',
      'title',
      "description",
      "due date",
      "created at",
      "is in progress",
      "is completed",
      "started at",
      "completed date"
    ];

    List<List<String>> listOfLists =
        todosListModel.values.map((e) => convertObjToStringList(e)).toList();
    exportCSV.myCSV(header, listOfLists);
  }

  static List<String> convertObjToStringList(Todo todo) {
    return [
      todo.id.toString(),
      todo.title,
      todo.description,
      todo.dueDate.toString(),
      todo.createdAt.toString(),
      todo.isInProgress.toString(),
      todo.isCompleted.toString(),
      todo.startedAt.toString(),
      todo.completedDate.toString()
    ];
  }
}
