import 'package:flutter/material.dart';

import 'app_themes.dart';

final inputDecoration = InputDecoration(
    border: OutlineInputBorder(
      borderSide: BorderSide.none,
      borderRadius: BorderRadius.circular(12),
    ),
    hintStyle: TextStyle(color: lightTheme.primaryColor),
    prefixIconColor: lightTheme.primaryColor,
    fillColor: Colors.grey.shade200,
    alignLabelWithHint: true,
    contentPadding: const EdgeInsets.all(5));
