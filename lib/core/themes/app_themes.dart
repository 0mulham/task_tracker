import 'package:flex_color_scheme/flex_color_scheme.dart';

enum AppTheme {
  light,
  dark,
}

final appThemeData = {AppTheme.light: lightTheme, AppTheme.dark: darkTheme};

final lightTheme = FlexThemeData.light(
  fontFamily: "Ubuntu",
  useMaterial3: true,
  scheme: FlexScheme.deepPurple,
);

final darkTheme = FlexThemeData.dark(
    fontFamily: "Ubuntu", useMaterial3: true, scheme: FlexScheme.deepPurple);

// .copyWith(
//     primaryColor: Colors.purple.shade300,
//     appBarTheme: AppBarTheme(
//       backgroundColor: Colors.purple.shade300,
//       titleTextStyle: const TextStyle(color: Colors.white, fontSize: 16),
//     ));
