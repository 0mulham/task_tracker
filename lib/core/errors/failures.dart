import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  const Failure([List properties = const <dynamic>[]]);
}

// General failures

class ServerFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class GeneralFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class CacheFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class CacheThemeFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class NoInternetConnectionFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class DeleteTodoFailure extends Failure {
  @override
  List<Object?> get props => [];
}

class UpdateTodoFailure extends Failure {
  @override
  List<Object?> get props => [];
}
