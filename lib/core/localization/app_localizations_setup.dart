import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart'
    as localizations;
import 'app_localizations.dart';

class AppLocalizationsSetup {
  static const Iterable<Locale> supportedLocales = [
    Locale('en'),
    Locale('ar'),
  ];

  static const Iterable<LocalizationsDelegate<dynamic>> localizationsDelegates =
      [
    AppLocalizations.delegate,
    localizations.GlobalMaterialLocalizations.delegate,
    localizations.GlobalWidgetsLocalizations.delegate,
    localizations.GlobalCupertinoLocalizations.delegate,
  ];

  static Locale localeResolutionCallback(
      Locale locale, Iterable<Locale> supportedLocales) {
    for (Locale supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode &&
          supportedLocale.countryCode == locale.countryCode) {
        return supportedLocale;
      }
    }
    return supportedLocales.first;
  }
}
