part of 'locale_bloc.dart';

abstract class LocaleState extends Equatable {
  const LocaleState();

  @override
  List<Object> get props => [];
}

class LocaleInitial extends LocaleState {
  @override
  List<Object> get props => [];
}

class HasLocale extends LocaleState {
  final Locale locale;
  const HasLocale(this.locale);

  @override
  List<Object> get props => [locale];
}
