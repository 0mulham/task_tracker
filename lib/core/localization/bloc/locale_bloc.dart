import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'locale_event.dart';
part 'locale_state.dart';

class LocaleBloc extends Bloc<LocaleEvent, LocaleState> {
  LocaleBloc() : super(LocaleInitial()) {
    on<ToArabic>((event, emit) {
      print("emit");
      emit(const HasLocale(Locale("ar")));
    });
    on<ToEnglish>((event, emit) {
      print("emit ee");

      emit(const HasLocale(Locale("en")));
    });
  }
}
