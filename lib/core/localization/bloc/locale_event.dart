part of 'locale_bloc.dart';

abstract class LocaleEvent extends Equatable {
  const LocaleEvent();

  @override
  List<Object> get props => [];
}

class ToArabic extends LocaleEvent {
  @override
  List<Object> get props => [];
}

class ToEnglish extends LocaleEvent {
  @override
  List<Object> get props => [];
}
