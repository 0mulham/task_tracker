import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tasks_tracker/core/localization/bloc/locale_bloc.dart';
import 'features/todos/domain/usecases/sync_todos.dart';
import 'features/todos/domain/usecases/toggle_status.dart';

import 'core/network/network_info.dart';
import 'features/auth/data/datasources/auth_datasource.dart';
import 'features/auth/data/repositories/user_respository_impl.dart';
import 'features/auth/domain/repositories/user_repositroy.dart';
import 'features/auth/domain/usecases/get_signed_user_usecase.dart';
import 'features/auth/domain/usecases/log_out_usecase.dart';
import 'features/auth/domain/usecases/sign_in_usecase.dart';
import 'features/auth/presentation/bloc/auth_bloc_bloc.dart';
import 'features/settings/data/datasources/customized_theme_datasource.dart';
import 'features/settings/data/repositories/customized_theme_repository_impl.dart';
import 'features/settings/domain/repositories/theme_repository.dart';
import 'features/settings/domain/usecases/change_theme_usecase.dart';
import 'features/settings/domain/usecases/get_theme_usecase.dart';
import 'features/settings/presentaion/bloc/customized_theme_bloc.dart';
import 'features/todos/data/datasources/todos_local_datasource.dart';
import 'features/todos/data/datasources/todos_remote_datasource.dart';
import 'features/todos/data/repositories/todo_repository_impl.dart';
import 'features/todos/domain/repositories/todo_repository.dart';
import 'features/todos/domain/usecases/create_todo.dart';
import 'features/todos/domain/usecases/delete_todo.dart';
import 'features/todos/domain/usecases/get_todos_list.dart';
import 'features/todos/domain/usecases/reset_todo.dart';
import 'features/todos/domain/usecases/update_todo.dart';
import 'features/todos/presentaion/bloc/todos_bloc.dart';

final serviceLocator = GetIt.instance;
Future<void> init() async {
  //dealing with Features -> todo feature
  initFeatures();

  // dealing with core functionalties
  initCore();
  //external
  await initExternal();
}

void initFeatures() {
  /// init  from top to down
  initTodoFeature();
  initSettingFeature();
  initAuthFeature();
  initLocaleFeature();
}

void initLocaleFeature() {
  serviceLocator.registerFactory(() => LocaleBloc());
}

void initCore() {
  serviceLocator.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImpl(serviceLocator()));
}

Future<void> initExternal() async {
  final sharedPreferences = await SharedPreferences.getInstance();
  serviceLocator.registerLazySingleton(() => sharedPreferences);

  final firebaseDatabase = FirebaseDatabase.instance;
  serviceLocator.registerLazySingleton(() => firebaseDatabase);
  final firebaseAuth = FirebaseAuth.instance;
  serviceLocator.registerLazySingleton(() => firebaseAuth);

  final internetConnectionChecker = InternetConnectionChecker();
  serviceLocator.registerFactory(() => internetConnectionChecker);
}

void initTodoFeature() {
  serviceLocator.registerFactory(() => TodosBloc(
        getTodoList: serviceLocator(),
        createTodo: serviceLocator(),
        resetTodo: serviceLocator(),
        deleteTodo: serviceLocator(),
        updateTodo: serviceLocator(),
        toggleStatus: serviceLocator(),
        syncTodos: serviceLocator(),
      ));

  /// regesiter as lazy singleton because we want to get the same service
  /// and usecase does not hold any state
  // register the usecases
  serviceLocator.registerFactory<TodoRepository>(() => TodoRepositoryImpl(
      networkInfo: serviceLocator(),
      todoLocalDataSource: serviceLocator(),
      todoRemoteDataSource: serviceLocator()));
  serviceLocator.registerLazySingleton(() => GetTodoList(serviceLocator()));
  serviceLocator.registerLazySingleton(() => CreateTodo(serviceLocator()));
  serviceLocator.registerLazySingleton(() => ResetTodo(serviceLocator()));
  serviceLocator.registerLazySingleton(() => DeleteTodo(serviceLocator()));
  serviceLocator.registerLazySingleton(() => UpdateTodo(serviceLocator()));
  serviceLocator.registerLazySingleton(() => ToggleStatus(serviceLocator()));
  serviceLocator.registerLazySingleton(() => SyncTodos(serviceLocator()));

  serviceLocator.registerLazySingleton<TodoRemoteDataSource>(
      () => TodoRemoteDataSourceImpl(firebaseDatabase: serviceLocator()));

  serviceLocator.registerLazySingleton<TodoLocalDataSource>(
      () => TodoLocalDataSourceImpl(sharedPreferences: serviceLocator()));
}

void initAuthFeature() {
  serviceLocator.registerLazySingleton(() => AuthBlocBloc(
      signIn: serviceLocator(),
      logoutUser: serviceLocator(),
      getSignedUser: serviceLocator()));

  serviceLocator.registerLazySingleton<UserRepository>(() => UserRepositroyImpl(
      authDataSource: serviceLocator(), networkInfo: serviceLocator()));
  serviceLocator.registerLazySingleton<AuthDataSource>(
      () => AuthDataSourceImpl(serviceLocator(), serviceLocator()));
  serviceLocator.registerLazySingleton(() => SignIn(serviceLocator()));

  serviceLocator.registerLazySingleton(() => Logout(serviceLocator()));

  serviceLocator.registerLazySingleton(() => GetSignedUser(serviceLocator()));
}

void initSettingFeature() {
  serviceLocator.registerLazySingleton(() => CustomizedThemeBloc(
        getTheme: serviceLocator(),
        changeTheme: serviceLocator(),
      ));

  /// regesiter as lazy singleton because we want to get the same service
  /// and usecase does not hold any state
  // register the usecases
  serviceLocator.registerLazySingleton<CustomizedThemeRepository>(
      () => CustomizedThemeRepositoryImpl(
            customizedThemeDataSource: serviceLocator(),
          ));
  serviceLocator.registerLazySingleton(() => GetTheme(serviceLocator()));
  serviceLocator.registerLazySingleton(() => ChangeTheme(serviceLocator()));

  serviceLocator.registerLazySingleton<CustomizedThemeDataSource>(
      () => CustomizedThemeDataSourceImpl(sharedPreferences: serviceLocator()));
}
