part of 'auth_bloc_bloc.dart';

abstract class AuthBlocState extends Equatable {
  const AuthBlocState();

  @override
  List<Object> get props => [];
}

class AuthBlocInitial extends AuthBlocState {}

class Logged extends AuthBlocState {
  final UserData userData;

  const Logged(this.userData);
}

class LoggingIn extends AuthBlocState {}

class NotLogged extends AuthBlocState {}

class LoginError extends AuthBlocState {
  final String message;
  const LoginError(this.message);
}

class LogoutError extends AuthBlocState {
  final String message;
  const LogoutError(this.message);
}
