part of 'auth_bloc_bloc.dart';

abstract class AuthBlocEvent extends Equatable {
  const AuthBlocEvent();

  @override
  List<Object> get props => [];
}

class LoginEvent extends AuthBlocEvent {}

class LogoutEvent extends AuthBlocEvent {}

class GetUserData extends AuthBlocEvent {}
