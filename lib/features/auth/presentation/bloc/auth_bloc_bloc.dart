// ignore_for_file: depend_on_referenced_packages

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/constants.dart';
import '../../../../core/errors/failures.dart';
import '../../../../core/params/no_params.dart';
import '../../domain/entities/user.dart';
import '../../domain/usecases/get_signed_user_usecase.dart';
import '../../domain/usecases/log_out_usecase.dart';
import '../../domain/usecases/sign_in_usecase.dart';

part 'auth_bloc_event.dart';
part 'auth_bloc_state.dart';

class AuthBlocBloc extends Bloc<AuthBlocEvent, AuthBlocState> {
  final SignIn signIn;
  final Logout logoutUser;
  final GetSignedUser getSignedUser;

  AuthBlocBloc(
      {required this.signIn,
      required this.logoutUser,
      required this.getSignedUser})
      : super(AuthBlocInitial()) {
    on<LoginEvent>(_mapLoginEventToState);
    on<LogoutEvent>(
      _mapLogoutEventToState,
    );

    on<GetUserData>(_mapGetUserDataEventToState);
  }

  FutureOr<void> _mapGetUserDataEventToState(event, emit) async {
    var result = await getSignedUser(NoParams());
    result.fold((l) => emit(const LoginError('User Not Found')),
        (r) => emit(Logged(r)));
  }

  FutureOr<void> _mapLogoutEventToState(event, emit) async {
    await logoutUser(NoParams());
    emit(NotLogged());
  }

  FutureOr<void> _mapLoginEventToState(event, emit) async {
    emit(LoggingIn());
    var sign = await signIn(NoParams());
    sign.fold(
        (failure) => emit(LoginError(_getCustomLoginFailureMessage(failure))),
        (r) => emit(Logged(r)));
  }

// NoInternetConnectionException
  String _getCustomLoginFailureMessage(Failure failure) {
    if (failure is NoInternetConnectionFailure) {
      return NO_INTERNET_CONNECTION_ERROR_MESSAGE;
    } else {
      return LOGIN_ERROR_MESSAGE;
    }
  }
}
