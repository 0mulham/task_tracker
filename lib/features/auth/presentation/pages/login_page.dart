import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/auth_bloc_bloc.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/images/login.gif',
                  width: 220,
                ),
                const Padding(
                  padding: EdgeInsets.all(14),
                  child: Text(
                    'Welcome To Task Tracker App',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 22),
                  ),
                ),
                BlocBuilder<AuthBlocBloc, AuthBlocState>(
                    builder: (context, state) {
                  if (state is LoggingIn) {
                    return Column(
                      children: const [
                        Text("Preparing things for you."),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: CircularProgressIndicator(),
                        ),
                      ],
                    );
                  }
                  return Column(
                    children: [
                      (state is LoginError)
                          ? Text(
                              state.message,
                              textAlign: TextAlign.center,
                              style: const TextStyle(color: Colors.red),
                            )
                          : Container(),
                      ElevatedButton(
                          onPressed: () {
                            BlocProvider.of<AuthBlocBloc>(context)
                                .add(LoginEvent());
                          },
                          child: const Text("Start doing your tasks")),
                    ],
                  );
                }),
              ]),
        ),
      ),
    );
  }
}
