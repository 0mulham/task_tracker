import 'package:dartz/dartz.dart';

import '../../../../core/errors/exceptions.dart';
import '../../../../core/errors/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../domain/entities/user.dart';
import '../../domain/repositories/user_repositroy.dart';
import '../datasources/auth_datasource.dart';

class UserRepositroyImpl implements UserRepository {
  final AuthDataSource authDataSource;

  final NetworkInfo networkInfo;
  UserRepositroyImpl({required this.authDataSource, required this.networkInfo});

  @override
  Future<Either<Failure, UserData>> getSignedUser() async {
    try {
      final user = await authDataSource.getSignedUser();
      return Right(user);
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, void>> logOutUser() async {
    authDataSource.logout();
    return Future.value();
  }

  @override
  Future<Either<Failure, UserData>> signInUser() async {
    try {
      if (networkInfo.isConnected) {
        var user = await authDataSource.signIn();
        return Right(user);
      } else {
        throw NoInternetConnectionException();
      }
    } on NoInternetConnectionException {
      return Left(NoInternetConnectionFailure());
    } catch (e) {
      return Left(ServerFailure());
    }
  }
}
