import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/constants.dart';
import '../../../../core/errors/exceptions.dart';
import '../../domain/entities/user.dart';
import '../models/user_model.dart';

abstract class AuthDataSource {
  Future<UserData> signIn();
  Future<void> logout();
  Future<UserData> getSignedUser();
}

class AuthDataSourceImpl extends AuthDataSource {
  final FirebaseAuth firebaseAuth;
  final SharedPreferences sharedPreferences;

  AuthDataSourceImpl(this.firebaseAuth, this.sharedPreferences);

  @override
  Future<UserDataModel> getSignedUser() async {
    try {
      var uid = sharedPreferences.getString(CACHED_USERID);
      if (uid == null) {
        throw CacheException();
      }
      return UserDataModel(userCredential: firebaseAuth.currentUser!);
    } catch (e) {
      throw Exception(e);
    }
  }

  @override
  Future<void> logout() async {
    try {
      firebaseAuth.signOut();
      sharedPreferences.remove(CACHED_USERID);
    } catch (e) {
      throw Exception(e);
    }
  }

  @override
  Future<UserData> signIn() async {
    try {
      final userCredential = await firebaseAuth.signInAnonymously();
      if (userCredential.user == null) {
        throw ServerException();
      }
      sharedPreferences.setString(
          CACHED_USERID, userCredential.user!.uid.toString());
      return UserData(userCredential: userCredential.user!);
    } catch (e) {
      throw Exception(e);
    }
  }
}
