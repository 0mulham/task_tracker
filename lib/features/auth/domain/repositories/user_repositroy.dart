import 'package:dartz/dartz.dart' as dartz;

import '../../../../core/errors/failures.dart';
import '../entities/user.dart';

abstract class UserRepository {
  Future<dartz.Either<Failure, UserData>> getSignedUser();
  Future<dartz.Either<Failure, UserData>> signInUser();

  Future<dartz.Either<Failure, void>> logOutUser();
}
