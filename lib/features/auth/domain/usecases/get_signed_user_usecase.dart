import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/params/no_params.dart';
import '../../../../core/usecases/usecase.dart';
import '../entities/user.dart';
import '../repositories/user_repositroy.dart';

class GetSignedUser implements UseCase<UserData, NoParams> {
  final UserRepository _userRepository;

  GetSignedUser(this._userRepository);
  @override
  Future<Either<Failure, UserData>> call(NoParams params) async {
    return await _userRepository.getSignedUser();
  }
}
