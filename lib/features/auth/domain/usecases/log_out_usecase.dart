import 'package:dartz/dartz.dart';
import '../../../../core/usecases/usecase.dart';
import '../repositories/user_repositroy.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/params/no_params.dart';

class Logout implements UseCase<void, NoParams> {
  final UserRepository _userRepository;

  Logout(this._userRepository);

  @override
  Future<Either<Failure, void>> call(NoParams params) async {
    return await _userRepository.logOutUser();
  }
}
