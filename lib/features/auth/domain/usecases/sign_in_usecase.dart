import 'package:dartz/dartz.dart' as dartz;

import '../../../../core/errors/failures.dart';
import '../../../../core/params/no_params.dart';
import '../../../../core/usecases/usecase.dart';
import '../entities/user.dart';
import '../repositories/user_repositroy.dart';

class SignIn implements UseCase<UserData, NoParams> {
  final UserRepository _userRepository;
  SignIn(this._userRepository);
  @override
  Future<dartz.Either<Failure, UserData>> call(NoParams params) async {
    return await _userRepository.signInUser();
  }
}
