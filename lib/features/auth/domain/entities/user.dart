import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';

class UserData extends Equatable {
  final User userCredential;
  const UserData({required this.userCredential});

  @override
  List<Object?> get props => [userCredential];
}
