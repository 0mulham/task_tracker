import 'package:flutter/material.dart';

import '../../../../core/localization/app_localizations.dart';
import '../../../todos/presentaion/widgets/shared/app_bar.dart';
import '../widgets/export_to_csv_widget.dart';
import '../widgets/change_theme_widget.dart';
import '../widgets/change_language_widget.dart';
import '../widgets/logout_button_widget.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: sharedAppBar(
          title: AppLocalizations.of(context)!
              .translate('setting_page_title')
              .toString()),
      body: Padding(
          padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 12),
          child: Column(children: const [
            ChangeThemeWidget(),
            ChangeLanguageWidget(),
            ExportToCsvWidget(),
            LogoutButtonWidget(),
          ])),
    );
  }
}
