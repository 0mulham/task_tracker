part of 'customized_theme_bloc.dart';

abstract class CustomizedThemeState extends Equatable {
  final CustomizedTheme customizedTheme;

  const CustomizedThemeState({required this.customizedTheme});

  @override
  List<Object> get props => [customizedTheme];
}

class CustomizedThemeInitial extends CustomizedThemeState {
  const CustomizedThemeInitial({required super.customizedTheme});
}

class CustomizedThemeLoaded extends CustomizedThemeState {
  const CustomizedThemeLoaded({required super.customizedTheme});
}
