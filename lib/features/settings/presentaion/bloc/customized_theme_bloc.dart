// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/params/customized_theme_params.dart';
import '../../../../core/params/no_params.dart';
import '../../../../core/themes/app_themes.dart';
import '../../data/models/customized_theme_model.dart';
import '../../domain/entities/customized_theme.dart';
import '../../domain/usecases/change_theme_usecase.dart';
import '../../domain/usecases/get_theme_usecase.dart';

part 'customized_theme_event.dart';
part 'customized_theme_state.dart';

class CustomizedThemeBloc
    extends Bloc<CustomizedThemeEvent, CustomizedThemeState> {
  final GetTheme getTheme;
  final ChangeTheme changeTheme;

  CustomizedThemeBloc({required this.changeTheme, required this.getTheme})
      : super(const CustomizedThemeInitial(
            customizedTheme: CustomizedTheme(theme: AppTheme.light))) {
    on<GetAppThemeEvent>(_getAppThemeEventToState);
    on<SetAppThemeEvent>(_setAppThemeEventToState);
  }

  Future<void> _getAppThemeEventToState(
      GetAppThemeEvent event, Emitter<CustomizedThemeState> emit) async {
    const defaultTheme = CustomizedTheme(theme: AppTheme.light);

    emit(const CustomizedThemeInitial(customizedTheme: defaultTheme));
    var getCurrent = await getTheme.execute(NoParams());

    getCurrent.fold((failure) {
      changeTheme.execute(CustomizedThemeParams(defaultTheme));
      emit(const CustomizedThemeLoaded(customizedTheme: defaultTheme));
    }, (theme) => emit(CustomizedThemeLoaded(customizedTheme: theme)));
  }

  Future<void> _setAppThemeEventToState(
      SetAppThemeEvent event, Emitter<CustomizedThemeState> emit) async {
    var theme = CustomizedThemeModel(theme: event.appTheme);
    changeTheme.execute(CustomizedThemeParams(theme));
    emit(CustomizedThemeLoaded(customizedTheme: theme));
  }
}
