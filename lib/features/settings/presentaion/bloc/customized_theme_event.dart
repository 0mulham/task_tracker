part of 'customized_theme_bloc.dart';

abstract class CustomizedThemeEvent extends Equatable {
  const CustomizedThemeEvent();

  @override
  List<Object> get props => [];
}

class GetAppThemeEvent extends CustomizedThemeEvent {
  @override
  List<Object> get props => [];
}

class SetAppThemeEvent extends CustomizedThemeEvent {
  final AppTheme appTheme;

  const SetAppThemeEvent({required this.appTheme});
  @override
  List<Object> get props => [appTheme];
}
