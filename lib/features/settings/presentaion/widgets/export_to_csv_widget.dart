import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tasks_tracker/core/utils/snackbars.dart';
import '../../../../core/localization/app_localizations.dart';
import '../../../../core/utils/csv_exporter.dart';
import '../../../auth/presentation/bloc/auth_bloc_bloc.dart';
import '../../../todos/presentaion/bloc/todos_bloc.dart';

class ExportToCsvWidget extends StatelessWidget {
  const ExportToCsvWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return BlocBuilder<AuthBlocBloc, AuthBlocState>(
        builder: (context, authState) =>
            BlocBuilder<TodosBloc, TodosState>(builder: (context, state) {
          return ListTile(
            title: Text(AppLocalizations.of(context)!
                .translate('export_to_csv_btn')
                .toString()),
            trailing: (state is LoadedTodos || state is Empty)
                ? const Icon(Icons.file_copy)
                : const CircularProgressIndicator(),
            onTap: () {
              if (state is Empty) {
                showExportError(context);
              }
              if (state is LoadedTodos) {
                if (state.todosList.values.isEmpty) {
                  showExportError(context);
                } else {
                  CsvExporterService.exportToCsv(state.todosList);
                }
              }
            },
          );
        }),
      );
    });
  }

  void showExportError(BuildContext context) {
    showErrorMessage(
        context,
        AppLocalizations.of(context)!
            .translate('no_data_to_export')
            .toString());
  }
}
