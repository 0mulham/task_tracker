import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/localization/app_localizations.dart';
import '../../../../core/themes/app_themes.dart';
import '../bloc/customized_theme_bloc.dart';

class ChangeThemeWidget extends StatelessWidget {
  const ChangeThemeWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CustomizedThemeBloc, CustomizedThemeState>(
        builder: (context, state) {
      return SwitchListTile(
        title: Text(
            AppLocalizations.of(context)!.translate('dark_theme').toString()),
        subtitle: state.customizedTheme.theme == AppTheme.dark
            ? Text(AppLocalizations.of(context)!
                .translate('dark_theme_selected_message')
                .toString())
            : Text(AppLocalizations.of(context)!
                .translate('light_theme_selected_message')
                .toString()),
        onChanged: (value) {
          if (state.customizedTheme.theme == AppTheme.dark) {
            BlocProvider.of<CustomizedThemeBloc>(context)
                .add(const SetAppThemeEvent(appTheme: AppTheme.light));
          } else {
            BlocProvider.of<CustomizedThemeBloc>(context)
                .add(const SetAppThemeEvent(appTheme: AppTheme.dark));
          }
        },
        value: state.customizedTheme.theme == AppTheme.dark ? true : false,
      );
    });
  }
}
