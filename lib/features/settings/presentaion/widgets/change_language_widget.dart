import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tasks_tracker/core/localization/app_localizations.dart';
import 'package:tasks_tracker/core/localization/bloc/locale_bloc.dart';

class ChangeLanguageWidget extends StatelessWidget {
  const ChangeLanguageWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(AppLocalizations.of(context)!
          .translate('change_language_btn')
          .toString()),
      trailing: const Icon(Icons.language),
      onTap: () {
        if (AppLocalizations.of(context) != null) {
          if (AppLocalizations.of(context)!.isEnLocale) {
            BlocProvider.of<LocaleBloc>(context).add(ToArabic());
          } else {
            BlocProvider.of<LocaleBloc>(context).add(ToEnglish());
          }
        }
      },
    );
  }
}
