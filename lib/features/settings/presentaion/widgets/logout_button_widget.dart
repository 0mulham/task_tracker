import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/localization/app_localizations.dart';
import '../../../auth/presentation/bloc/auth_bloc_bloc.dart';

class LogoutButtonWidget extends StatelessWidget {
  const LogoutButtonWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        BlocProvider.of<AuthBlocBloc>(context).add(LogoutEvent());
      },
      title: Text(AppLocalizations.of(context)!
          .translate('logout_btn_title')
          .toString()),
      subtitle: Text(
          AppLocalizations.of(context)!.translate('logout_warning').toString()),
    );
  }
}
