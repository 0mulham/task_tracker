import 'dart:core';

import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/constants.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/themes/app_themes.dart';
import '../../domain/entities/customized_theme.dart';
import '../models/customized_theme_model.dart';

abstract class CustomizedThemeDataSource {
  /// if no todos found throws [CacheException]
  Future<CustomizedThemeModel> getCurrentTheme();
  Future<bool> setCurrentTheme(CustomizedTheme appTheme);
}

class CustomizedThemeDataSourceImpl implements CustomizedThemeDataSource {
  final SharedPreferences sharedPreferences;

  CustomizedThemeDataSourceImpl({required this.sharedPreferences});
  @override
  Future<CustomizedThemeModel> getCurrentTheme() async {
    try {
      var result =
          sharedPreferences.getString(CACHED_THEME).toString().toLowerCase();
      var theme = AppTheme.light;
      if (result == "dark") {
        theme = AppTheme.dark;
      }
      return Future.value(CustomizedThemeModel(theme: theme));
    } catch (e) {
      throw CacheException();
    }
  }

  @override
  Future<bool> setCurrentTheme(CustomizedTheme appTheme) async {
    var result = await sharedPreferences.setString(
        CACHED_THEME, appTheme.theme.name.toLowerCase());

    if (result) {
      return result;
    } else {
      throw CacheException();
    }
  }
}
