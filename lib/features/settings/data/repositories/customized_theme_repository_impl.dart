import 'package:dartz/dartz.dart';

import '../../../../core/errors/exceptions.dart';
import '../../../../core/errors/failures.dart';
import '../../domain/entities/customized_theme.dart';
import '../../domain/repositories/theme_repository.dart';
import '../datasources/customized_theme_datasource.dart';

class CustomizedThemeRepositoryImpl extends CustomizedThemeRepository {
  final CustomizedThemeDataSource customizedThemeDataSource;

  CustomizedThemeRepositoryImpl({required this.customizedThemeDataSource});
  @override
  void changeTheme(CustomizedTheme appTheme) {
    customizedThemeDataSource.setCurrentTheme(appTheme);
  }

  @override
  Future<Either<Failure, CustomizedTheme>> getCurrentTheme() async {
    try {
      var result = await customizedThemeDataSource.getCurrentTheme();
      return Right(result);
    } on CacheException {
      return Left(CacheThemeFailure());
    }
  }
}
