import '../../domain/entities/customized_theme.dart';

class CustomizedThemeModel extends CustomizedTheme {
  const CustomizedThemeModel({required super.theme});
}
