import 'package:equatable/equatable.dart';

import '../../../../core/themes/app_themes.dart';

class CustomizedTheme extends Equatable {
  final AppTheme theme;

  const CustomizedTheme({required this.theme});
  @override
  List<Object?> get props => [theme];
}
