import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../entities/customized_theme.dart';

abstract class CustomizedThemeRepository {
  void changeTheme(CustomizedTheme appTheme);

  Future<Either<Failure, CustomizedTheme>> getCurrentTheme();
}
