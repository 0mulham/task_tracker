import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/params/no_params.dart';
import '../../../../core/usecases/theme_usecase.dart';
import '../entities/customized_theme.dart';
import '../repositories/theme_repository.dart';

class GetTheme
    implements
        ThemeUsecase<Future<Either<Failure, CustomizedTheme>>, NoParams> {
  CustomizedThemeRepository repository;
  GetTheme(this.repository);

  @override
  Future<Either<Failure, CustomizedTheme>> execute(NoParams noParams) async {
    return repository.getCurrentTheme();
  }
}
