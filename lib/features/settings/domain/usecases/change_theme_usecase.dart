import '../../../../core/params/customized_theme_params.dart';
import '../../../../core/usecases/theme_usecase.dart';
import '../repositories/theme_repository.dart';

class ChangeTheme implements ThemeUsecase<void, CustomizedThemeParams> {
  CustomizedThemeRepository repository;
  ChangeTheme(this.repository);

  @override
  void execute(CustomizedThemeParams appTheme) {
    repository.changeTheme(appTheme.customizedTheme);
  }
}
