import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../../core/localization/app_localizations.dart';
import '../../../../core/params/toggel_status_params.dart';
import '../../../../core/utils/date_formatter.dart';
import '../../domain/entities/todo.dart';
import '../bloc/todos_bloc.dart';
import '../pages/edit_todo_page.dart';

import '../../../auth/presentation/bloc/auth_bloc_bloc.dart';

class TodoItemWidget extends StatelessWidget {
  final Todo todoModel;

  const TodoItemWidget({super.key, required this.todoModel});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            TaskItem(todoModel: todoModel),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                TaskItemBtnShimmer(
                  todoModel: todoModel,
                ),
                DeleteTodoButton(id: todoModel.id.toString()),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class TaskItem extends StatelessWidget {
  final Todo todoModel;
  const TaskItem({super.key, required this.todoModel});

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // TaskIconMapperWidget(status: todoModel.todoStatus),
        Expanded(
          child: SizedBox(
              width: double.infinity,
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ListTile(
                      title: Text(todoModel.title,
                          style:
                              TextStyle(color: Theme.of(context).primaryColor)),
                      subtitle:
                          Text(todoModel.description, style: const TextStyle()),
                    ),

                    GridView(
                        shrinkWrap: true,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2, childAspectRatio: 3),
                        children: [
                          ListTile(
                            title: Text(
                              AppLocalizations.of(context)!
                                  .translate('should_end')
                                  .toString(),
                            ),
                            subtitle: Text(
                                " ${DateFormatter.format(todoModel.dueDate)}"),
                          ),
                          todoModel.startedAt == null
                              ? Container()
                              : ListTile(
                                  title: Text(AppLocalizations.of(context)!
                                      .translate('started_at')
                                      .toString()),
                                  subtitle: Text(
                                      "${DateFormatter.format(todoModel.startedAt!)}"),
                                ),
                          todoModel.completedDate == null
                              ? Container()
                              : ListTile(
                                  title: Text(AppLocalizations.of(context)!
                                      .translate('completed_at')
                                      .toString()),
                                  subtitle: Text(
                                      "${DateFormatter.format(todoModel.completedDate!)}"),
                                ),
                          todoModel.completedDate == null ||
                                  todoModel.startedAt == null
                              ? Container()
                              : ListTile(
                                  title: Text(AppLocalizations.of(context)!
                                      .translate('completed_in')
                                      .toString()),
                                  subtitle: Text(DateFormatter.getDuration(
                                      todoModel.startedAt!,
                                      todoModel.completedDate!)),
                                ),
                        ]),

                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 2),
                      child: SizedBox(
                        height: 50,
                        child: BlocBuilder<TodosBloc, TodosState>(
                            builder: (context, todosBlocState) {
                          return Row(
                            // shrinkWrap: true,
                            // physics: BouncingScrollPhysics(),
                            // scrollDirection: Axis.horizontal,
                            children: [
                              _buildStatusSelector(context, todosBlocState,
                                  todo: todoModel,
                                  color: Colors.orange,
                                  icon: Icons.schedule,
                                  type: TodoStatus.todo,
                                  title: AppLocalizations.of(context)!
                                      .translate('todo')
                                      .toString()),
                              _buildStatusSelector(context, todosBlocState,
                                  todo: todoModel,
                                  color: Colors.green,
                                  icon: Icons.circle_outlined,
                                  type: TodoStatus.inProgress,
                                  title: AppLocalizations.of(context)!
                                      .translate('in_progress')
                                      .toString()),
                              _buildStatusSelector(context, todosBlocState,
                                  color: Colors.red,
                                  todo: todoModel,
                                  type: TodoStatus.finished,
                                  icon: Icons.check,
                                  title: AppLocalizations.of(context)!
                                      .translate('finished')
                                      .toString()),
                            ],
                          );
                        }),
                      ),
                    )
                    // TodoTypesWidget()
                  ],
                ),
              )),
        ),
      ],
    );
  }

  Widget _buildStatusSelector(BuildContext context, TodosState bloc,
      {required Color color,
      required Todo todo,
      required IconData icon,
      required String title,
      required TodoStatus type}) {
    return Expanded(
      child: GestureDetector(
        onTap: () {
          var authState = BlocProvider.of<AuthBlocBloc>(context).state;
          BlocProvider.of<TodosBloc>(context).add(ToggleStatusEvent(
              ToggleStatusParams(
                  id: todoModel.id,
                  status: type,
                  uid: authState is Logged
                      ? authState.userData.userCredential.uid
                      : "")));
        },
        child: Container(
          height: 40,
          width: 80,
          margin: const EdgeInsets.symmetric(horizontal: 8.0),
          decoration: BoxDecoration(
              color: todoModel.todoStatus == type ? color : Colors.transparent,
              border: Border.all(color: color),
              borderRadius: BorderRadius.circular(10)),
          child: FittedBox(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(children: [
                Icon(
                  Icons.schedule,
                  color: todoModel.todoStatus == type ? Colors.white : color,
                ),
                const SizedBox(
                  width: 10,
                ),
                Text(
                  title,
                  style: TextStyle(
                      color:
                          todoModel.todoStatus == type ? Colors.white : color),
                ),
              ]),
            ),
          ),
        ),
      ),
    );
  }
}

class TaskIconMapperWidget extends StatelessWidget {
  TaskIconMapperWidget({super.key, required this.status});
  final TodoStatus status;
  final icon = {
    TodoStatus.todo: Icons.schedule,
    TodoStatus.finished: Icons.check,
    TodoStatus.inProgress: Icons.circle_outlined,
  };
  final color = {
    TodoStatus.todo: Colors.orange,
    TodoStatus.finished: Colors.red,
    TodoStatus.inProgress: Colors.green,
  };
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50,
      height: 50,
      margin: const EdgeInsets.only(top: 10),
      decoration: const BoxDecoration(
        shape: BoxShape.circle,
      ),
      child: Icon(
        icon[status],
        color: color[status],
      ),
    );
  }
}

class TaskItemBtnShimmer extends StatelessWidget {
  const TaskItemBtnShimmer({super.key, required this.todoModel});
  final Todo todoModel;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ElevatedButton(
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (_) => EditTodoPage(todoModel: todoModel)));
          },
          child: Text(
              AppLocalizations.of(context)!.translate('update').toString())),
    );
  }
}

class DeleteTodoButton extends StatelessWidget {
  final String id;

  const DeleteTodoButton({super.key, required this.id});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ElevatedButton(
          onPressed: () {
            var auth = BlocProvider.of<AuthBlocBloc>(context);
            var todosBLoc = BlocProvider.of<TodosBloc>(context);

            final authState = auth.state;

            _showConfirmationDialog(context, authState, todosBLoc);
          },
          child: Text(
              AppLocalizations.of(context)!.translate('remove').toString())),
    );
  }

  Future<dynamic> _showConfirmationDialog(
      BuildContext context, AuthBlocState authState, TodosBloc todosBLoc) {
    return showDialog(
      context: context,
      useRootNavigator: true,
      builder: (context) {
        return AlertDialog(
          title: Text(AppLocalizations.of(context)!
              .translate('delete_message')
              .toString()),
          actions: [
            ElevatedButton(
                onPressed: () {
                  if (authState is Logged) {
                    todosBLoc.add(DeleteTodoEvent(
                        id, authState.userData.userCredential.uid.toString()));
                  }
                  Navigator.of(context).pop();
                },
                child: Text(
                    AppLocalizations.of(context)!.translate('yes').toString())),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                    AppLocalizations.of(context)!.translate('no').toString()))
          ],
        );
      },
    );
  }
}
