import 'package:flutter/material.dart';
import 'package:tasks_tracker/core/localization/app_localizations.dart';

import '../pages/create_todo_page.dart';

class NoTodosWidget extends StatelessWidget {
  const NoTodosWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          Icons.emoji_flags_sharp,
          size: 70,
          color: Theme.of(context).primaryColor,
        ),
        Text(
          AppLocalizations.of(context)!
              .translate("no_todos_message")
              .toString(),
          textAlign: TextAlign.center,
          style: const TextStyle(fontSize: 22),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: FloatingActionButton.extended(
              key: Key("startnowkey"),
              label: Text(AppLocalizations.of(context)!
                  .translate('start_now')
                  .toString()),
              onPressed: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (_) => const CreateTodoPage()));
              },
              icon: const Icon(Icons.create)),
        ),
      ],
    );
  }
}
