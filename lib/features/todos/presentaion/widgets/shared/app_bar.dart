import 'package:flutter/material.dart';

AppBar sharedAppBar({String? title, Icon? icon}) {
  return AppBar(
    title: Text(title == null ? "Task Tracker" : title.toString()),
    leading: icon ??
        const Icon(
          Icons.task,
        ),
  );
}
