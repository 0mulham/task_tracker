import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class TaskItemLoader extends StatelessWidget {
  const TaskItemLoader({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 2),
      child: Shimmer.fromColors(
        baseColor: Colors.grey.shade300,
        highlightColor: Colors.grey.shade100,
        period: const Duration(milliseconds: 1300),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            const TaskItemLoaderShimmer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: const [
                TaskItemBtnShimmer(),
                TaskItemBtnShimmer(),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class TaskItemLoaderShimmer extends StatelessWidget {
  const TaskItemLoaderShimmer({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: 50,
          height: 50,
          margin: const EdgeInsets.only(top: 10),
          decoration:
              const BoxDecoration(shape: BoxShape.circle, color: Colors.white),
        ),
        const Expanded(
          child: SizedBox(height: 90, width: double.infinity, child: Card()),
        ),
      ],
    );
  }
}

class TaskItemBtnShimmer extends StatelessWidget {
  const TaskItemBtnShimmer({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ElevatedButton(
          onPressed: () {},
          child: Container(
            width: 40,
          )),
    );
  }
}
