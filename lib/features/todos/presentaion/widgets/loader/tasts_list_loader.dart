import 'package:flutter/material.dart';
import 'task_loader_item.dart';

class TasksListLoader extends StatelessWidget {
  const TasksListLoader({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const ScrollPhysics(),
      itemBuilder: (context, i) => const TaskItemLoader(),
      itemCount: 2,
    );
  }
}
