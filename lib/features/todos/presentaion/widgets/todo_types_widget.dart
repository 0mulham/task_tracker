import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/localization/app_localizations.dart';
import '../../../auth/presentation/bloc/auth_bloc_bloc.dart';
import '../../domain/entities/todo.dart';
import '../bloc/todos_bloc.dart';

class TodoTypesWidget extends StatelessWidget {
  const TodoTypesWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthBlocBloc, AuthBlocState>(
      builder: (context, authState) => SizedBox(
        height: 80,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: ListView(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              children: [
                GestureDetector(
                  onTap: () {
                    BlocProvider.of<TodosBloc>(context).add(GetTodoListEvent(
                        authState is Logged
                            ? authState.userData.userCredential.uid
                            : ""));
                  },
                  child: TypeItem(
                    color: Theme.of(context).primaryColor,
                    icon: Icons.event,
                    title: AppLocalizations.of(context)!
                        .translate('all')
                        .toString(),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    BlocProvider.of<TodosBloc>(context).add(
                        GetCompletedTodoList(
                            authState is Logged
                                ? authState.userData.userCredential.uid
                                : "",
                            TodoStatus.todo));
                  },
                  child: TypeItem(
                    color: Colors.orange,
                    icon: Icons.schedule,
                    title: AppLocalizations.of(context)!
                        .translate('todo')
                        .toString(),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    BlocProvider.of<TodosBloc>(context).add(
                        GetCompletedTodoList(
                            authState is Logged
                                ? authState.userData.userCredential.uid
                                : "",
                            TodoStatus.inProgress));
                  },
                  child: TypeItem(
                    color: Colors.green,
                    icon: Icons.circle_outlined,
                    title: AppLocalizations.of(context)!
                        .translate('in_progress')
                        .toString(),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    BlocProvider.of<TodosBloc>(context).add(
                        GetCompletedTodoList(
                            authState is Logged
                                ? authState.userData.userCredential.uid
                                : "",
                            TodoStatus.finished));
                  },
                  child: TypeItem(
                    color: Colors.red,
                    icon: Icons.done,
                    title: AppLocalizations.of(context)!
                        .translate('finished')
                        .toString(),
                  ),
                ),
              ]),
        ),
      ),
    );
  }
}

class TypeItem extends StatelessWidget {
  final String title;
  final IconData icon;
  final Color color;

  const TypeItem(
      {super.key,
      required this.title,
      required this.icon,
      required this.color});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 40,
        decoration: BoxDecoration(
            border: Border.all(color: color),
            borderRadius: BorderRadius.circular(10)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(children: [
            Icon(
              icon,
              color: color,
            ),
            const SizedBox(
              width: 10,
            ),
            Text(
              title,
              style: TextStyle(color: color),
            ),
          ]),
        ),
      ),
    );
  }
}
