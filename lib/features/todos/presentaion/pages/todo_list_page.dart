import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tasks_tracker/core/localization/app_localizations.dart';
import '../../../../core/utils/snackbars.dart';

import '../../../auth/presentation/bloc/auth_bloc_bloc.dart';
import '../bloc/todos_bloc.dart';
import '../widgets/loader/tasts_list_loader.dart';
import '../widgets/no_todos_widget.dart';
import '../widgets/shared/app_bar.dart';
import '../widgets/todo_item_widget.dart';
import '../widgets/todo_types_widget.dart';
import 'create_todo_page.dart';

class TodoListPage extends StatefulWidget {
  const TodoListPage({super.key});

  @override
  State<TodoListPage> createState() => _TodoListPageState();
}

class _TodoListPageState extends State<TodoListPage> {
  String uid = "aa";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton.extended(
          label: Text(AppLocalizations.of(context)!
              .translate('new_task_btn')
              .toString()),
          onPressed: () {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (_) => const CreateTodoPage()));
          },
          icon: const Icon(Icons.create)),
      appBar: sharedAppBar(
        title: AppLocalizations.of(context)!
            .translate('home_app_bar_title')
            .toString(),
      ),
      body: BlocBuilder<TodosBloc, TodosState>(builder: (context, todosState) {
        if (todosState is SyncingTodos) {
          return Text(
            AppLocalizations.of(context)!.translate('snycing').toString(),
            style: const TextStyle(color: Colors.orange),
          );
        }
        return BlocBuilder<AuthBlocBloc, AuthBlocState>(
          builder: (context, authState) => BlocListener<TodosBloc, TodosState>(
            listener: (context, state) {
              if (state is TodoCreated) {
                BlocProvider.of<TodosBloc>(context).add(GetTodoListEvent(
                    authState is Logged
                        ? authState.userData.userCredential.uid
                        : uid));
              }

              if (state is TodoUpdated) {
                showSuccessMessage(context, state.message);
                BlocProvider.of<TodosBloc>(context).add(GetTodoListEvent(
                    authState is Logged
                        ? authState.userData.userCredential.uid
                        : uid));
              }
            },
            child: SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.only(
                    bottom: kFloatingActionButtonMargin +
                        kBottomNavigationBarHeight),
                child: Center(
                  child: _buildBasedOnState(todosState),
                ),
              ),
            ),
          ),
        );
      }),
    );
  }

  Column _buildBasedOnState(TodosState state) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const TodoTypesWidget(),
        (state is LoadingTodos)
            ? const TasksListLoader()
            : (state is LoadedTodos)
                ? state.todosList.values.isEmpty
                    ? const NoTodosWidget()
                    : ListView.builder(
                        cacheExtent:
                            (4 * state.todosList.values.length).toDouble(),
                        physics: const ScrollPhysics(),
                        shrinkWrap: true,
                        itemBuilder: (context, i) => TodoItemWidget(
                            todoModel: state.todosList.values[i]),
                        itemCount: state.todosList.values.length,
                      )
                : state is TodoError
                    ? Text(state.errorMessage)
                    : const NoTodosWidget()
      ],
    );
  }
}
