import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart' as picker;

import '../../../../core/localization/app_localizations.dart';
import '../../../../core/themes/text_field_theme.dart';
import '../../../../core/utils/snackbars.dart';
import '../../../auth/presentation/bloc/auth_bloc_bloc.dart';
import '../../data/models/todo_model.dart';
import '../bloc/todos_bloc.dart';
import '../widgets/shared/app_bar.dart';

class CreateTodoPage extends StatefulWidget {
  const CreateTodoPage({super.key});

  @override
  State<CreateTodoPage> createState() => _CreateTodoPageState();
}

class _CreateTodoPageState extends State<CreateTodoPage> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  DateTime? dueDate;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: sharedAppBar(
          title: AppLocalizations.of(context)!
              .translate('create_todo_page_title')
              .toString(),
          icon: const Icon(Icons.create)),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: BlocConsumer<TodosBloc, TodosState>(
            listener: (context, state) {
              if (state is TodoError) {
                showErrorMessage(context, state.errorMessage);
              }
              if (state is TodoCreated) {
                showSuccessMessage(context, state.message);
                resetValues();
              }
            },
            builder: (context, todoState) =>
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              _buildTitleTextField(),
              _buildDescriptionTextField(),
              _buildDueDateWidget(context),
              const SizedBox(
                height: 40,
              ),
              _buildButtonsArea(context, todoState)
            ]),
          ),
        ),
      )),
    );
  }

  Padding _buildButtonsArea(BuildContext context, TodosState state) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          _buildCancelBtn(context),
          const SizedBox(
            width: 10,
          ),
          _buildSaveBtn(state, context),
        ],
      ),
    );
  }

  Row _buildDueDateWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        ElevatedButton(
            onPressed: () async {
              var date = await picker.DatePicker.showDateTimePicker(
                context,
              );
              if (date != null) {
                setState(() {
                  dueDate = date;
                });
              }
            },
            child: Text(AppLocalizations.of(context)!
                .translate('select_due_date')
                .toString())),
        Text(dueDate == null
            ? AppLocalizations.of(context)!
                .translate('due_date_not_set')
                .toString()
            : dueDate.toString())
      ],
    );
  }

  ElevatedButton _buildCancelBtn(BuildContext context) {
    return ElevatedButton(
        style: ButtonStyle(
            backgroundColor:
                MaterialStateColor.resolveWith((states) => Colors.redAccent)),
        onPressed: () async {
          Navigator.of(context).pop();
        },
        child: Text(
          AppLocalizations.of(context)!.translate('cancel').toString(),
          style: TextStyle(color: Colors.white),
        ));
  }

  ElevatedButton _buildSaveBtn(TodosState state, BuildContext context) {
    return ElevatedButton(
        onPressed: () => (state is CreatingTodo) ? () {} : _createTodo(context),
        child: Row(
          children: [
            Text(
              (state is CreatingTodo)
                  ? AppLocalizations.of(context)!
                      .translate('creating_todo')
                      .toString()
                  : AppLocalizations.of(context)!
                      .translate('create_todo')
                      .toString(),
            ),
          ],
        ));
  }

  void _createTodo(BuildContext context) {
    if (dueDate == null ||
        _descriptionController.text.isEmpty ||
        _titleController.text.isEmpty) {
      showErrorMessage(
          context,
          AppLocalizations.of(context)!
              .translate('provide_all_data_message')
              .toString());
    } else {
      var auth = BlocProvider.of<AuthBlocBloc>(context);

      final authState = auth.state;
      if (authState is Logged) {
        BlocProvider.of<TodosBloc>(context).add(CreateTodoEvent(
            TodoModel(
                id: "00",
                createdAt: DateTime.now(),
                description: _descriptionController.text,
                title: _titleController.text,
                dueDate: dueDate!),
            authState.userData.userCredential.uid));
      } else {
        showErrorMessage(context, "Not Authenticated");
      }
    }
  }

  Padding _buildDescriptionTextField() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: TextField(
          style: const TextStyle(color: Colors.black),
          controller: _descriptionController,
          maxLines: 6,
          minLines: 2,
          decoration: inputDecoration.copyWith(
            hintText: AppLocalizations.of(context)!
                .translate('description')
                .toString(),
            prefixIcon: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Icon(Icons.text_snippet_rounded),
            ),
          )),
    );
  }

  Padding _buildTitleTextField() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: TextField(
          style: const TextStyle(color: Colors.black),
          controller: _titleController,
          decoration: inputDecoration.copyWith(
            hintText:
                AppLocalizations.of(context)!.translate('title').toString(),
            prefixIcon: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Icon(Icons.text_fields_rounded),
            ),
          )),
    );
  }

  void resetValues() {
    _titleController.text = "";
    _descriptionController.text = "";
    dueDate = null;
    setState(() {});
  }
}
