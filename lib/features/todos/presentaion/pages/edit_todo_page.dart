import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart' as picker;
import '../../../../core/localization/app_localizations.dart';
import '../../domain/entities/todo.dart';

import '../../../../core/themes/text_field_theme.dart';
import '../../../../core/utils/snackbars.dart';
import '../../../auth/presentation/bloc/auth_bloc_bloc.dart';
import '../bloc/todos_bloc.dart';
import '../widgets/shared/app_bar.dart';

class EditTodoPage extends StatefulWidget {
  const EditTodoPage({super.key, required this.todoModel});
  final Todo todoModel;
  @override
  State<EditTodoPage> createState() => _EditTodoPageState();
}

class _EditTodoPageState extends State<EditTodoPage> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  DateTime? dueDate;

  @override
  void initState() {
    _titleController.text = widget.todoModel.title;
    _descriptionController.text = widget.todoModel.description;
    dueDate = widget.todoModel.dueDate;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: sharedAppBar(
          title: AppLocalizations.of(context)!
              .translate('update_todo_page_title')
              .toString(),
          icon: Icon(Icons.create)),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: BlocListener<TodosBloc, TodosState>(
              listener: (context, state) {
                if (state is TodoError) {
                  showErrorMessage(context, state.errorMessage);
                }
                if (state is TodoUpdated) {
                  showSuccessMessage(context, state.message);
                }
              },
              child: BlocBuilder<TodosBloc, TodosState>(
                builder: (context, state) => Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _buildTitleTextField(),
                      _buildDescriptionTextField(),
                      _buildDueDateWidget(context),
                      const SizedBox(
                        height: 40,
                      ),
                      _buildButtonsArea(context, state)
                    ]),
              )),
        ),
      )),
    );
  }

  Padding _buildButtonsArea(BuildContext context, TodosState state) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          _buildCancelBtn(context),
          const SizedBox(
            width: 10,
          ),
          _buildSaveBtn(state, context),
        ],
      ),
    );
  }

  Row _buildDueDateWidget(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        ElevatedButton(
            onPressed: () async {
              var date = await picker.DatePicker.showDateTimePicker(
                currentTime: dueDate,
                context,
              );
              if (date != null) {
                setState(() {
                  dueDate = date;
                });
              }
            },
            child: Text(AppLocalizations.of(context)!
                .translate('select_due_date')
                .toString())),
        Text(dueDate == null
            ? AppLocalizations.of(context)!
                .translate('due_date_not_set')
                .toString()
            : dueDate.toString())
      ],
    );
  }

  ElevatedButton _buildCancelBtn(BuildContext context) {
    return ElevatedButton(
        style: ButtonStyle(
            backgroundColor:
                MaterialStateColor.resolveWith((states) => Colors.redAccent)),
        onPressed: () async {
          Navigator.of(context).pop();
        },
        child: Text(
          AppLocalizations.of(context)!.translate('cancel').toString(),
          style: TextStyle(color: Colors.white),
        ));
  }

  ElevatedButton _buildSaveBtn(TodosState state, BuildContext context) {
    return ElevatedButton(
        onPressed: () => (state is UpdatingTodo) ? () {} : _updateTodo(context),
        child: Row(
          children: [
            Text(
              (state is UpdatingTodo)
                  ? AppLocalizations.of(context)!
                      .translate('updating')
                      .toString()
                  : AppLocalizations.of(context)!
                      .translate('update_todo_btn')
                      .toString(),
            ),
          ],
        ));
  }

  void _updateTodo(BuildContext context) {
    if (dueDate == null ||
        _descriptionController.text.isEmpty ||
        _titleController.text.isEmpty) {
      showErrorMessage(
          context,
          AppLocalizations.of(context)!
              .translate('provide_all_data_message')
              .toString());
    } else {
      var auth = BlocProvider.of<AuthBlocBloc>(context);

      final authState = auth.state;
      if (authState is Logged) {
        BlocProvider.of<TodosBloc>(context).add(UpdateTodoEvent(
            widget.todoModel.copyWith(
                title: _titleController.text,
                description: _descriptionController.text,
                dueDate: dueDate),
            authState.userData.userCredential.uid));
      } else {
        showErrorMessage(context, "Not Authenticated");
      }
    }
  }

  Padding _buildDescriptionTextField() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: TextField(
          style: const TextStyle(color: Colors.black),
          controller: _descriptionController,
          maxLines: 6,
          minLines: 2,
          decoration: inputDecoration.copyWith(
            hintText: "Description",
            prefixIcon: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Icon(Icons.text_snippet_rounded),
            ),
          )),
    );
  }

  Padding _buildTitleTextField() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: TextField(
          style: const TextStyle(color: Colors.black),
          controller: _titleController,
          decoration: inputDecoration.copyWith(
            hintText: "Title",
            prefixIcon: const Padding(
              padding: EdgeInsets.all(8.0),
              child: Icon(Icons.text_fields_rounded),
            ),
          )),
    );
  }
}
