import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../../core/params/toggel_status_params.dart';
import '../../domain/usecases/sync_todos.dart';
import '../../../../core/params/create_todo_params.dart';
import '../../../../core/params/delete_todo_params.dart';
import '../../../../core/params/update_todo_params.dart';
import '../../data/models/todo_list_model.dart';
import '../../domain/entities/todo.dart';
import '../../domain/usecases/delete_todo.dart';
import '../../domain/usecases/reset_todo.dart';
import '../../domain/usecases/toggle_status.dart';
import '../../domain/usecases/update_todo.dart';
import '../../../../core/params/no_params.dart';
import '../../domain/entities/todos_list.dart';
import '../../domain/usecases/create_todo.dart';
import '../../domain/usecases/get_todos_list.dart';

import '../../../../core/constants.dart';
import '../../../../core/errors/failures.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';

part 'todos_event.dart';
part 'todos_state.dart';

class TodosBloc extends Bloc<TodosEvent, TodosState> {
  final GetTodoList getTodoList;
  final CreateTodo createTodo;
  final ResetTodo resetTodo;
  final DeleteTodo deleteTodo;
  final UpdateTodo updateTodo;
  final ToggleStatus toggleStatus;
  final SyncTodos syncTodos;
  // late StreamSubscription<Either<Failure, TodosListModel>>
  //     _todoStreamSubscription;
  TodosBloc(
      {required this.getTodoList,
      required this.createTodo,
      required this.resetTodo,
      required this.deleteTodo,
      required this.updateTodo,
      required this.toggleStatus,
      required this.syncTodos})
      : super(Empty()) {
    on<GetTodoListEvent>(_getTodoListEventToState, transformer: restartable());
    on<CreateTodoEvent>(_createTodoEventToState);
    on<ResetTodosEvent>(_resetTodosEventToState);
    on<DeleteTodoEvent>(_deleteTodoEventToState);
    on<TodoListUpdatedEvent>(_todoListUpdatedEventToState);
    on<UpdateTodoEvent>(_updateTodoEventToState);
    on<ToggleStatusEvent>(_toggleStatusEventToState);
    on<SyncOfflineWithOnlineTodos>(
      _mapSyncingEventToState,
    );
    on<GetCompletedTodoList>(_getListByTodoStatusToState,
        transformer: restartable());
  }

  FutureOr<void> _mapSyncingEventToState(event, emit) async {
    emit(SyncingTodos());
    var syncTodoCall = await syncTodos.execute(event.uid);

    syncTodoCall.fold(
        (l) => emit(SyncingTodosError()), (r) => emit(SyncingTodosCompleted()));
  }

  FutureOr<void> _getListByTodoStatusToState(event, emit) async {
    emit(LoadingTodos());
    var gettodo = getTodoList.execute(event.uid);

    await for (final data in gettodo) {
      data.fold((l) {
        emit(TodoError(errorMessage: _mapFailureToMessage(l)));
      }, (r) {
        var todo = r.values
            .where((element) => element.todoStatus == event.status)
            .toList();

        add(TodoListUpdatedEvent(TodosList(values: todo)));
      });
    }
  }

  Future<void> _toggleStatusEventToState(
      ToggleStatusEvent event, Emitter<TodosState> emit) async {
    await toggleStatus.execute(event.toggleStatusParams);

    // update.fold((l) => _mapFailureToMessage(l), (r) {
    // print("updated");
    add(GetTodoListEvent(event.toggleStatusParams.uid));
    emit(const TodoUpdated(message: TODO_STATUS_UPDATED));
    // });
  }

  Future<void> _updateTodoEventToState(
      UpdateTodoEvent event, Emitter<TodosState> emit) async {
    emit(UpdatingTodo());
    var update = await updateTodo.execute(UpdateTodoParams(
        uid: event.uid,
        description: event.todoModel.description,
        id: event.todoModel.id,
        title: event.todoModel.title,
        dueDate: event.todoModel.dueDate));

    update.fold((l) => _mapFailureToMessage(l), (r) {
      emit(const TodoUpdated(message: TODO_UPDATED_MESSAGE));
    });
  }

  Future<void> _todoListUpdatedEventToState(
      TodoListUpdatedEvent event, Emitter<TodosState> emit) async {
    emit(LoadedTodos(todosList: event.todoListModel));
  }

  Future<void> _deleteTodoEventToState(
      DeleteTodoEvent event, Emitter<TodosState> emit) async {
    var delete =
        await deleteTodo.execute(DeleteTodoParams(event.id, event.uid));

    delete.fold((failure) {
      emit(TodoError(errorMessage: _mapFailureToMessage(failure)));
    }, (todoList) {
      emit(const TodoDeleted(message: TODO_DELETED_MESSAGE));
      add(GetTodoListEvent(event.uid));
    });
  }

  Future<void> _resetTodosEventToState(
      ResetTodosEvent event, Emitter<TodosState> emit) async {
    emit(Empty());

    await resetTodo.execute(NoParams());
  }

  Future<void> _getTodoListEventToState(
      GetTodoListEvent event, Emitter<TodosState> emit) async {
    // _todoStreamSubscription.cancel();
    emit(LoadingTodos());
    // print("getting ");
    var getData = getTodoList.execute(event.uid);
    await for (final data in getData) {
      data.fold(
          (l) => _mapFailureToMessage(l), (r) => add(TodoListUpdatedEvent(r)));
    }

    // getData.fold(
    //     (failure) =>
    //         emit(TodoError(errorMessage: _mapFailureToMessage(failure))),
    //     (todoList) => emit(LoadedTodos(todosList: todoList)));
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return GET_TODOS_SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return GET_TODOS_CACHE_FAILURE_MESSAGE;

      case DeleteTodoFailure:
        return DELETE_TODO_FAILURE_MESSAGE;
      case UpdateTodoFailure:
        return UPDATE_TODO_FAILURE_MESSAGE;
      default:
        return UNEXPECTED_ERROR;
    }
  }

  FutureOr<void> _createTodoEventToState(
      CreateTodoEvent event, Emitter<TodosState> emit) async {
    emit(CreatingTodo());
    var created = await createTodo.execute(CreateTodoParams(
        id: DateTime.now().toString(),
        description: event.todoModel.description,
        title: event.todoModel.title,
        dueDate: event.todoModel.dueDate,
        uid: event.userUid));
    created.fold(
        (failure) =>
            emit(TodoError(errorMessage: _mapFailureToMessage(failure))),
        (todoList) {
      emit(const TodoCreated(message: TODO_CREATED_MESSAGE));
      // add(GetTodoListEvent(event.userUid));
    });
  }
}
