part of 'todos_bloc.dart';

abstract class TodosState extends Equatable {
  const TodosState();

  @override
  List<Object> get props => [];
}

class Empty extends TodosState {
  @override
  List<Object> get props => [];
}

class LoadingTodos extends TodosState {
  @override
  List<Object> get props => [];
}

class LoadedTodos extends TodosState {
  final TodosList todosList;
  const LoadedTodos({required this.todosList});
  @override
  List<Object> get props => [todosList];
}

class TodoError extends TodosState {
  final String errorMessage;

  const TodoError({required this.errorMessage});
  @override
  List<Object> get props => [errorMessage];
}

class CreatingTodo extends TodosState {
  @override
  List<Object> get props => [];
}

class TodoCreated extends TodosState {
  final String message;

  const TodoCreated({required this.message});
  @override
  List<Object> get props => [];
}

class UpdatingTodo extends TodosState {
  @override
  List<Object> get props => [];
}

class TodoUpdated extends TodosState {
  final String message;

  const TodoUpdated({required this.message});
  @override
  List<Object> get props => [message];
}

class TodoDeleted extends TodosState {
  final String message;

  const TodoDeleted({required this.message});
  @override
  List<Object> get props => [message];
}

class TodoListUpdated extends TodosState {
  final TodosListModel todosList;

  const TodoListUpdated({required this.todosList});
  @override
  List<Object> get props => [todosList];
}

class SyncingTodos extends TodosState {
  @override
  List<Object> get props => [];
}

class SyncingTodosCompleted extends TodosState {
  @override
  List<Object> get props => [];
}

class SyncingTodosError extends TodosState {
  @override
  List<Object> get props => [];
}
