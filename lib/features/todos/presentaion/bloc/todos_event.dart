part of 'todos_bloc.dart';

abstract class TodosEvent extends Equatable {
  const TodosEvent();

  @override
  List<Object> get props => [];
}

class GetTodoListEvent extends TodosEvent {
  final String uid;

  const GetTodoListEvent(this.uid);
  @override
  List<Object> get props => [uid];
}

class TodoListUpdatedEvent extends TodosEvent {
  final TodosList todoListModel;

  const TodoListUpdatedEvent(this.todoListModel);
  @override
  List<Object> get props => [todoListModel];
}

class CreateTodoEvent extends TodosEvent {
  final Todo todoModel;
  final String userUid;
  const CreateTodoEvent(this.todoModel, this.userUid);
  @override
  List<Object> get props => [todoModel];
}

class ResetTodosEvent extends TodosEvent {
  @override
  List<Object> get props => [];
}

class DeleteTodoEvent extends TodosEvent {
  final String id;
  final String uid;

  const DeleteTodoEvent(this.id, this.uid);

  @override
  List<Object> get props => [id, uid];
}

class UpdateTodoEvent extends TodosEvent {
  final Todo todoModel;
  final String uid;

  const UpdateTodoEvent(this.todoModel, this.uid);

  @override
  List<Object> get props => [todoModel, uid];
}

class ToggleStatusEvent extends TodosEvent {
  final ToggleStatusParams toggleStatusParams;
  const ToggleStatusEvent(this.toggleStatusParams);
  @override
  List<Object> get props => [toggleStatusParams];
}

class GetCompletedTodoList extends TodosEvent {
  final String uid;
  final TodoStatus status;
  const GetCompletedTodoList(this.uid, this.status);
  @override
  List<Object> get props => [uid, status];
}

class SyncOfflineWithOnlineTodos extends TodosEvent {
  final String uid;
  const SyncOfflineWithOnlineTodos(this.uid);
  @override
  List<Object> get props => [];
}
