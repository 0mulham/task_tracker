import 'dart:async';

import 'package:dartz/dartz.dart';
import '../../../../core/params/toggel_status_params.dart';
import '../models/todo_list_model.dart';
import '../models/todo_model.dart';

import '../../../../core/errors/exceptions.dart';
import '../../../../core/errors/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../domain/entities/todos_list.dart';
import '../../domain/repositories/todo_repository.dart';
import '../datasources/todos_local_datasource.dart';
import '../datasources/todos_remote_datasource.dart';

class TodoRepositoryImpl implements TodoRepository {
  final TodoRemoteDataSource todoRemoteDataSource;
  final TodoLocalDataSource todoLocalDataSource;
  final NetworkInfo networkInfo;
  final StreamController<TodosListModel> _todosStramController =
      StreamController();

  TodoRepositoryImpl(
      {required this.networkInfo,
      required this.todoLocalDataSource,
      required this.todoRemoteDataSource});
  @override
  Future<Either<Failure, TodoModel>> createTodo(
      {required String id,
      required String title,
      required String description,
      required DateTime dueDate,
      required String uid}) async {
    if (networkInfo.isConnected) {
      try {
        final remoteData = await todoRemoteDataSource.createTodo(
            id: id,
            title: title,
            description: description,
            dueDate: dueDate,
            uid: uid);
        todoLocalDataSource.addSingleTodoToCache(TodoModel(
            id: id,
            description: description,
            title: title,
            createdAt: DateTime.now(),
            dueDate: dueDate));
        return Right(remoteData);
      } on ServerException {
        return Left(ServerFailure());
      } catch (e) {
        return Left(GeneralFailure());
      }
    } else {
      try {
        final createdTodo = TodoModel(
            id: id,
            description: description,
            title: title,
            createdAt: DateTime.now(),
            dueDate: dueDate);
        todoLocalDataSource.addSingleTodoToCache(createdTodo);
        // todoLocalDataSource.addTodoToCache(remoteData);
        return Right(createdTodo);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, bool>> deleteTodo(
      {required String id, required String uid}) async {
    try {
      todoRemoteDataSource.deleteTodo(id: id, uid: uid);

      return Right(await todoLocalDataSource.deleteTodo(id: id));
    } on Exception {
      return Left(DeleteTodoFailure());
    }
  }

  @override
  Stream<Either<Failure, TodosList>> getTodosList(String uid) async* {
    if (networkInfo.isConnected == false) {
      try {
        final localData = await todoLocalDataSource.getTodosList();
        yield Right(localData);
      } on CacheException {
        yield Left(CacheFailure());
      }
    } else {
      try {
        final remoteData = todoRemoteDataSource.getTodosList(uid);

        await for (final data in remoteData) {
          // todoLocalDataSource.addTodoToCache(data);
          yield Right(data);
        }
      } on ServerException {
        yield Left(ServerFailure());
      }
    }
  }

  @override
  Future<Either<Failure, bool>> updateTodo(
      {required String title,
      required String description,
      required DateTime dueDate,
      required String id,
      required String uid}) async {
    if (networkInfo.isConnected) {
      try {
        final update = await todoRemoteDataSource.updateTodo(
            id: id,
            title: title,
            description: description,
            dueDate: dueDate,
            uid: uid);
        return Right(update);
      } on ServerException {
        return Left(UpdateTodoFailure());
      } catch (e) {
        return Left(UpdateTodoFailure());
      }
    } else {
      try {
        final update = await todoLocalDataSource.updateTodo(
            id: id, title: title, description: description, dueDate: dueDate);
        return Right(update);
      } on CacheException {
        return Left(UpdateTodoFailure());
      }
    }
  }

  @override
  Future<Either<Failure, bool>> resetTodo() async {
    try {
      var removedWithUserId = await todoLocalDataSource.reset();

      todoRemoteDataSource.reset(removedWithUserId);

      return const Right(true);
    } catch (error) {
      return Left(CacheFailure());
    }
  }

  @override
  void dispose() {
    _todosStramController.close();
  }

  @override
  Future<void> toggleStatus(ToggleStatusParams toggleStatusParams) async {
    todoRemoteDataSource.toggleStatus(toggleStatusParams);

    todoLocalDataSource.toggleStatus(toggleStatusParams);
  }

  @override
  Future<Either<Failure, void>> syncTodos(String uid) async {
    var todos = await todoLocalDataSource.getTodosList();

    var synced = await todoRemoteDataSource.syncTodos(todos, uid);
    if (synced) {
      return Right(Future.value());
    } else {
      return Left(ServerFailure());
    }
  }
}
