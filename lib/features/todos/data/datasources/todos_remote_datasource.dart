import 'dart:async';
import 'dart:core';

import 'package:firebase_database/firebase_database.dart';
import '../../domain/entities/todos_list.dart';

import '../../../../core/errors/exceptions.dart';
import '../../../../core/params/toggel_status_params.dart';
import '../../domain/entities/todo.dart';
import '../models/todo_list_model.dart';
import '../models/todo_model.dart';

abstract class TodoRemoteDataSource {
  /// get data from remote server
  /// throws a [ServerException]
  ///
  ///
  Stream<TodosListModel> getTodosList(String uid);
  Future<TodosListModel> getCompletedTodosList();
  Future<void> toggleStatus(ToggleStatusParams toggleStatusParams);
  Future<TodoModel> createTodo(
      {required final String id,
      required final String title,
      required final String description,
      required final DateTime dueDate,
      required String uid});

  Future<bool> updateTodo(
      {required final String id,
      required final String title,
      required final String description,
      required final DateTime dueDate,
      required String uid});

  Future<bool> deleteTodo({
    required final String id,
    required final String uid,
  });
  Future<void> reset(String uid);
  Stream<TodosListModel> get todoListStream;

  Future<bool> syncTodos(TodosList todosList, String uid);
}

class TodoRemoteDataSourceImpl implements TodoRemoteDataSource {
  final FirebaseDatabase firebaseDatabase;
  final StreamController<TodosListModel> todosStreamController =
      StreamController.broadcast();

  TodoRemoteDataSourceImpl({
    required this.firebaseDatabase,
  });
  @override
  Future<TodoModel> createTodo(
      {required String id,
      required String title,
      required String description,
      required DateTime dueDate,
      required String uid}) async {
    var todoModel = TodoModel(
        id: id,
        description: description,
        title: title,
        createdAt: DateTime.now(),
        dueDate: dueDate);
    try {
      await firebaseDatabase
          .ref(uid)
          .child(id.replaceAll('.', ''))
          .set(todoModel.toJson());
      return todoModel;
    } catch (error) {
      throw Exception(error);
    }
  }

  @override
  Future<bool> deleteTodo(
      {required String id, required final String uid}) async {
    await firebaseDatabase.ref(uid).child(id.replaceAll('.', '')).remove();
    return true;
  }

  @override
  Future<TodosListModel> getCompletedTodosList() {
    // TODO: implement getCompletedTodosList
    throw UnimplementedError();
  }

  @override
  Stream<TodosListModel> getTodosList(String uid) async* {
    // final snapshot = await firebaseDatabase.ref(uid).get();
    DatabaseReference todosRef = firebaseDatabase.ref(uid);

    await for (final val in todosRef.onValue) {
      Map data = val.snapshot.value as Map;
      if (val.snapshot.value == null) {
        yield const TodosListModel(values: []);
        throw ServerException();
      }
      List<Map<String, dynamic>> finalData = [];
      data.forEach((key, value) {
        finalData.add(Map<String, dynamic>.from(value));
      });

      if (finalData.isNotEmpty) {
        yield TodosListModel(
            values: finalData.map((e) => TodoModel.fromJson(e)).toList());

        // if (todosStreamController.isClosed) {
        //   todosStreamController.sink.add(TodosListModel(
        //       values: finalData.map((e) => TodoModel.fromJson(e)).toList()));
        // } else {
        //   todosStreamController.add(TodosListModel(
        //       values: finalData.map((e) => TodoModel.fromJson(e)).toList()));
        // }
      } else {
        throw ServerException();
      }
    }
  }

  @override
  Stream<TodosListModel> get todoListStream => todosStreamController.stream;

  @override
  Future<bool> updateTodo(
      {required final String id,
      required String title,
      required String description,
      required DateTime dueDate,
      required String uid}) async {
    try {
      await firebaseDatabase.ref(uid).child(id.replaceAll('.', '')).update({
        'title': title,
        "description": description,
        'dueDate': dueDate.toString()
      });

      return true;
    } catch (e) {
      throw ServerException();
    }
  }

  @override
  Future<void> reset(String uid) async {
    firebaseDatabase.ref(uid).remove();
  }

  @override
  Future<void> toggleStatus(ToggleStatusParams toggleStatusParams) async {
    bool isCompleted = false;
    bool inProgress = false;
    DateTime? completedDate;
    DateTime? startedAt;
    switch (toggleStatusParams.status) {
      case TodoStatus.todo:
        isCompleted = false;
        inProgress = false;

        break;
      case TodoStatus.inProgress:
        isCompleted = false;
        inProgress = true;
        startedAt = DateTime.now();
        break;
      case TodoStatus.finished:
        isCompleted = true;
        inProgress = false;
        completedDate = DateTime.now();
        break;
    }
    if (toggleStatusParams.status == TodoStatus.finished) {
      await firebaseDatabase
          .ref(toggleStatusParams.uid)
          .child(toggleStatusParams.id.replaceAll('.', ''))
          .update({
        'isCompleted': isCompleted,
        "isInProgress": inProgress,
        'completedDate': completedDate.toString(),
      });
    } else {
      await firebaseDatabase
          .ref(toggleStatusParams.uid)
          .child(toggleStatusParams.id.replaceAll('.', ''))
          .update({
        'isCompleted': isCompleted,
        "isInProgress": inProgress,
        'completedDate': completedDate.toString(),
        'startedAt': startedAt.toString()
      });
    }
  }

  @override
  Future<bool> syncTodos(TodosList todosList, String uid) async {
    try {
      todosList.values.forEach((element) async {
        await firebaseDatabase
            .ref(uid)
            .child(element.id.replaceAll('.', ''))
            .set(element.toJson());
      });
      return true;
    } catch (e) {
      throw ServerException();
    }
  }
}
