import 'dart:convert';
import 'dart:core';

import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/constants.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/params/toggel_status_params.dart';
import '../../domain/entities/todo.dart';
import '../models/todo_list_model.dart';
import '../models/todo_model.dart';

abstract class TodoLocalDataSource {
  /// if no todos found throws [CacheException]
  Future<TodosListModel> getTodosList();
  // Future<TodosListModel> getCompletedTodosList();
  Future<void> toggleStatus(ToggleStatusParams toggleStatusParams);
  Future<void> addTodoToCache(TodosListModel todoList);
  Future<void> addSingleTodoToCache(TodoModel todoModel);
  Future<String> reset();

  // Future<Todo> createTodo({
  //   required final String title,
  //   required final String description,
  // });

  Future<bool> updateTodo(
      {required final String id,
      required final String title,
      required final String description,
      required final DateTime dueDate});

  Future<bool> deleteTodo({
    required final String id,
  });
}

class TodoLocalDataSourceImpl implements TodoLocalDataSource {
  final SharedPreferences sharedPreferences;

  const TodoLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<void> addTodoToCache(TodosListModel todoList) async {
    try {
      final stringTodos = json.encode(todoList.toJson());
      await sharedPreferences.setString(CACHED_TODOS, stringTodos);
      // return Future.value();
    } catch (e) {
      throw CacheException();
    }
  }

  @override
  Future<TodosListModel> getTodosList() {
    final jsonString = sharedPreferences.getString(CACHED_TODOS);
    if (jsonString == null) {
      throw CacheException();
    }
    return Future.value(
        TodosListModel.fromJson(json.decode(jsonString.toString())['values']));
  }

  @override
  Future<void> addSingleTodoToCache(TodoModel todoModel) {
    final jsonString = sharedPreferences.getString(CACHED_TODOS);
    if (jsonString == null) {
      var todos = TodosListModel(values: [todoModel]);
      return addTodoToCache(todos);
    } else {
      var todos =
          TodosListModel.fromJson(json.decode(jsonString.toString())['values']);
      todos.values.add(todoModel);
      return addTodoToCache(todos);
    }
  }

  @override
  Future<String> reset() async {
    sharedPreferences.remove(CACHED_TODOS);
    return sharedPreferences.getString(CACHED_USERID).toString();
  }

  @override
  Future<bool> deleteTodo({required String id}) async {
    var gettodos = await getTodosList();
    gettodos.values.removeWhere((element) => element.id == id);
    addTodoToCache(gettodos);
    return true;
  }

  @override
  Future<bool> updateTodo(
      {required String id,
      required String title,
      required String description,
      required DateTime dueDate}) async {
    var gettodos = await getTodosList();
    var updateIndex = gettodos.values.indexWhere((element) => element.id == id);
    if (updateIndex != -1) {
      TodoModel todo = gettodos.values[updateIndex].copyWith(
        dueDate: dueDate,
        title: title,
        description: description,
      );
      gettodos.values[updateIndex] = todo;

      await addTodoToCache(gettodos);
      return true;
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> toggleStatus(ToggleStatusParams toggleStatusParams) async {
    var gettodos = await getTodosList();

    var updateIndex = gettodos.values
        .indexWhere((element) => element.id == toggleStatusParams.id);
    if (updateIndex != -1) {
      bool isCompleted = false;
      bool inProgress = false;
      DateTime? completedDate;
      DateTime? startedAt = gettodos.values[updateIndex].startedAt;

      switch (toggleStatusParams.status) {
        case TodoStatus.todo:
          isCompleted = false;
          inProgress = false;

          break;
        case TodoStatus.inProgress:
          isCompleted = false;
          inProgress = true;
          startedAt = DateTime.now();
          break;
        case TodoStatus.finished:
          isCompleted = true;
          inProgress = false;

          completedDate = DateTime.now();
          break;
      }

      TodoModel todo = gettodos.values[updateIndex].copyWith(
        createdAt: startedAt,
        completedDate: completedDate,
        isInProgress: inProgress,
        isCompleted: isCompleted,
      );

      gettodos.values[updateIndex] = todo;
      var updated = gettodos.values;

      await addTodoToCache(TodosListModel(values: updated));
    } else {
      throw CacheException();
    }
  }
}
