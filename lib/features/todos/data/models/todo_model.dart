import '../../domain/entities/todo.dart';

class TodoModel extends Todo {
  const TodoModel(
      {required super.id,
      required super.description,
      required super.title,
      super.completedDate,
      super.isCompleted,
      super.isInProgress,
      super.startedAt,
      required super.createdAt,
      required super.dueDate})
      : super();

  @override
  Map<String, dynamic> toJson() {
    return {
      "id": id.replaceAll('.', ''),
      "title": title,
      "description": description,
      "isCompleted": isCompleted,
      "isInProgress": isInProgress,
      "dueDate": dueDate.toString(),
      "startedAt": startedAt.toString(),
      "createdAt": createdAt.toString(),
      "completedDate": completedDate.toString()
    };
  }

  factory TodoModel.fromJson(Map<String, dynamic> json) {
    return TodoModel(
      id: json["id"].toString(),
      description: json['description'],
      title: json['title'],
      createdAt: DateTime.parse(json['createdAt'].toString()),
      completedDate: json['completedDate'].toString() == "null"
          ? null
          : DateTime.parse(json['completedDate'].toString()),
      startedAt: json['startedAt'].toString() == "null"
          ? null
          : DateTime.parse(json['startedAt'].toString()),
      dueDate: DateTime.parse(json['dueDate'].toString()),
      isCompleted: (json['isCompleted']),
      isInProgress: (json['isInProgress']),
    );
  }
  @override
  TodoModel copyWith(
      {String? id,
      String? title,
      String? description,
      bool? isCompleted,
      bool? isInProgress,
      DateTime? completedDate,
      DateTime? startedAt,
      DateTime? createdAt,
      DateTime? dueDate}) {
    return TodoModel(
        id: id ?? this.id,
        createdAt: createdAt ?? this.createdAt,
        description: description ?? this.description,
        title: title ?? this.title,
        isCompleted: isCompleted ?? this.isCompleted,
        isInProgress: isInProgress ?? this.isInProgress,
        completedDate: completedDate,
        dueDate: dueDate ?? this.dueDate,
        startedAt: startedAt);
  }
}
