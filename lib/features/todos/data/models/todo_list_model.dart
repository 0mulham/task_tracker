import '../../domain/entities/todos_list.dart';
import 'todo_model.dart';

class TodosListModel extends TodosList {
  @override
  final List<TodoModel> values;
  const TodosListModel({required this.values}) : super(values: values);

  factory TodosListModel.fromJson(List<dynamic> json) {
    return TodosListModel(
        values: List.from(json).map((e) => TodoModel.fromJson(e)).toList());
  }

  Map<String, dynamic> toJson() {
    return {'values': values.map((e) => (e).toJson()).toList()};
  }
}
