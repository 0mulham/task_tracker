import 'package:dartz/dartz.dart' as dartz;
import '../../../../core/params/toggel_status_params.dart';

import '../../../../core/errors/failures.dart';
import '../entities/todo.dart';
import '../entities/todos_list.dart';

abstract class TodoRepository {
  Stream<dartz.Either<Failure, TodosList>> getTodosList(String uid);

  Future<void> toggleStatus(ToggleStatusParams toggleStatusParams);

  // Future<dartz.Either<Failure, TodosList>> getCompletedTodosList();

  Future<dartz.Either<Failure, Todo>> createTodo(
      {required final String id,
      required final String title,
      required final String description,
      required DateTime dueDate,
      required String uid});

  Future<dartz.Either<Failure, bool>> updateTodo(
      {required final String title,
      required final String id,
      required final String description,
      required final DateTime dueDate,
      required String uid});

  Future<dartz.Either<Failure, bool>> deleteTodo(
      {required final String id, required final String uid});

  Future<dartz.Either<Failure, bool>> resetTodo();

  Future<dartz.Either<Failure, void>> syncTodos(String uid);

  void dispose();
}
