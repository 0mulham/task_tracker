import 'package:equatable/equatable.dart';

enum TodoStatus { todo, inProgress, finished }

class Todo extends Equatable {
  final String id;
  final String title;
  final String description;
  final DateTime createdAt;
  final bool isCompleted;
  final bool isInProgress;
  final DateTime? completedDate;
  final DateTime? startedAt;
  final DateTime dueDate;

  TodoStatus get todoStatus {
    if (isCompleted) {
      return TodoStatus.finished;
    } else {
      if (isInProgress) {
        return TodoStatus.inProgress;
      } else {
        return TodoStatus.todo;
      }
    }
  }

  const Todo(
      {required this.id,
      required this.description,
      required this.title,
      required this.createdAt,
      this.isCompleted = false,
      this.isInProgress = false,
      this.completedDate,
      required this.dueDate,
      this.startedAt});

  @override
  List<Object?> get props => [
        id,
        title,
        description,
        createdAt,
        isCompleted,
        isInProgress,
        dueDate,
        startedAt,
        completedDate
      ];

  Todo setInProgress() =>
      copyWith(isInProgress: true, startedAt: DateTime.now());

  Todo complete() => copyWith(
      isInProgress: false, isCompleted: true, completedDate: DateTime.now());

  Todo copyWith(
      {String? id,
      String? title,
      String? description,
      bool? isCompleted,
      bool? isInProgress,
      DateTime? completedDate,
      DateTime? startedAt,
      DateTime? createdAt,
      DateTime? dueDate}) {
    return Todo(
        id: id ?? this.id,
        createdAt: createdAt ?? this.createdAt,
        description: description ?? this.description,
        title: title ?? this.title,
        isCompleted: isCompleted ?? this.isCompleted,
        isInProgress: isInProgress ?? this.isInProgress,
        completedDate: completedDate ?? this.completedDate,
        dueDate: dueDate ?? this.dueDate,
        startedAt: startedAt ?? this.startedAt);
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id.replaceAll('.', ''),
      "title": title,
      "description": description,
      "isCompleted": isCompleted,
      "isInProgress": isInProgress,
      "dueDate": dueDate.toString(),
      "startedAt": startedAt.toString(),
      "createdAt": createdAt.toString(),
      "completedDate": completedDate.toString()
    };
  }
}
