import 'package:equatable/equatable.dart';

import 'todo.dart';

class TodosList extends Equatable {
  final List<Todo> values;
  const TodosList({required this.values});

  operator [](final int index) => values[index];

  int get length => values.length;

  @override
  List<Object?> get props => [values];

  // TasksList addTodo(final Todo todo) => copyWith(values: [...values, todo]);

  // TasksList updateTodo(final Todo newTodo) {
  //   return copyWith(
  //       values: values
  //           .map((todo) => newTodo.id == todo.id ? newTodo : todo)
  //           .toList());
  // }

  // TasksList removeTodoById(final TodoId id) =>
  //     copyWith(values: values.where((todo) => todo.id != id).toList());

  // TasksList filterByCompleted() =>
  //     copyWith(values: values.where((todo) => todo.isCompleted).toList());

  // TasksList filterByIncomplete() =>
  //     copyWith(values: values.where((todo) => !todo.isCompleted).toList());
}
