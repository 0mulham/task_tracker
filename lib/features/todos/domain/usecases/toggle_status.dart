import '../../../../core/params/toggel_status_params.dart';
import '../../../../core/usecases/todo_usecase.dart';
import '../entities/todo.dart';
import '../repositories/todo_repository.dart';

class ToggleStatus implements ToggleStatusUseCase<Todo, ToggleStatusParams> {
  TodoRepository repository;
  ToggleStatus(this.repository);

  @override
  Future<void> execute(ToggleStatusParams params) async {
    return await repository.toggleStatus(params);
  }
}
