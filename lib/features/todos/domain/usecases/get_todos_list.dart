import 'package:dartz/dartz.dart' as dartz;

import '../../../../core/errors/failures.dart';
import '../../../../core/usecases/todo_usecase.dart';
import '../entities/todos_list.dart';
import '../repositories/todo_repository.dart';

class GetTodoList implements GetTodoUseCase<TodosList, String> {
  final TodoRepository _repository;

  const GetTodoList(this._repository);

  @override
  Stream<dartz.Either<Failure, TodosList>> execute(String uid) {
    return _repository.getTodosList(uid);
  }
}
