import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/params/no_params.dart';
import '../../../../core/usecases/todo_usecase.dart';
import '../repositories/todo_repository.dart';

class ResetTodo implements TodoUseCase<void, NoParams> {
  TodoRepository repository;
  ResetTodo(this.repository);

  @override
  Future<Either<Failure, void>> execute(NoParams params) async {
    return await repository.resetTodo();
  }
}
