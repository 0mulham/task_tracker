import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/params/delete_todo_params.dart';
import '../../../../core/usecases/todo_usecase.dart';
import '../repositories/todo_repository.dart';

class DeleteTodo implements TodoUseCase<bool, DeleteTodoParams> {
  TodoRepository repository;
  DeleteTodo(this.repository);

  @override
  Future<Either<Failure, bool>> execute(DeleteTodoParams params) async {
    return await repository.deleteTodo(id: params.id, uid: params.uid);
  }
}
