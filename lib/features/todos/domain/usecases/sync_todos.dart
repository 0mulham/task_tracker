import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/usecases/todo_usecase.dart';
import '../repositories/todo_repository.dart';

class SyncTodos implements TodoUseCase<void, String> {
  TodoRepository repository;
  SyncTodos(this.repository);

  @override
  Future<Either<Failure, void>> execute(String uid) async {
    return await repository.syncTodos(uid);
  }
}
