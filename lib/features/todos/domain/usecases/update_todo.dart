import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/params/update_todo_params.dart';
import '../../../../core/usecases/todo_usecase.dart';
import '../repositories/todo_repository.dart';

class UpdateTodo implements TodoUseCase<bool, UpdateTodoParams> {
  TodoRepository repository;
  UpdateTodo(this.repository);

  @override
  Future<Either<Failure, bool>> execute(UpdateTodoParams params) async {
    return await repository.updateTodo(
        title: params.title,
        uid: params.uid,
        id: params.id,
        description: params.description,
        dueDate: params.dueDate);
  }
}
