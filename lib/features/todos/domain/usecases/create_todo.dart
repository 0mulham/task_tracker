import 'package:dartz/dartz.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/params/create_todo_params.dart';
import '../../../../core/usecases/todo_usecase.dart';
import '../entities/todo.dart';
import '../repositories/todo_repository.dart';

class CreateTodo implements TodoUseCase<Todo, CreateTodoParams> {
  TodoRepository repository;
  CreateTodo(this.repository);

  @override
  Future<Either<Failure, Todo>> execute(CreateTodoParams params) async {
    return await repository.createTodo(
      id: params.id,
      title: params.title,
      description: params.description,
      dueDate: params.dueDate,
      uid: params.uid,
    );
  }
}
