import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../core/localization/app_localizations.dart';
import '../features/todos/presentaion/bloc/todos_bloc.dart';
import '../navbar_navigation/bloc/navbar_navigation_bloc.dart';
import '../navbar_navigation/data/navbar_items.dart';

import '../features/auth/presentation/bloc/auth_bloc_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NavbarNavigationBloc>(
      create: (context) => NavbarNavigationBloc(),
      child: Builder(builder: (context) {
        return BlocListener<AuthBlocBloc, AuthBlocState>(
          listener: (context, state) {
            if (state is Logged == false) {
              BlocProvider.of<TodosBloc>(context).add(ResetTodosEvent());
            }
          },
          child: BlocBuilder<NavbarNavigationBloc, NavbarNavigationState>(
              builder: (context, state) {
            return Scaffold(
                // appBar: sharedAppBar(),
                bottomNavigationBar: BottomNavigationBar(
                  items: [
                    BottomNavigationBarItem(
                        icon: const Icon(Icons.home),
                        label: AppLocalizations.of(context)!
                            .translate('home_nav_title')
                            .toString()),
                    BottomNavigationBarItem(
                        icon: Icon(Icons.settings),
                        label: AppLocalizations.of(context)!
                            .translate('setting_nav_title')
                            .toString())
                  ],
                  onTap: (value) =>
                      BlocProvider.of<NavbarNavigationBloc>(context)
                          .add(NavbarIndexChanged(value)),
                  currentIndex: state.index,
                ),
                body: navigationTabs[state.navbarItem]!);
          }),
        );
      }),
    );
  }
}
