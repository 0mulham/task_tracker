import '../../features/settings/presentaion/pages/settings_page.dart';
import '../../features/todos/presentaion/pages/todo_list_page.dart';

enum NavbarItem { home, settings }

final navigationTabs = {
  NavbarItem.home: const TodoListPage(),
  NavbarItem.settings: const SettingsPage(),
};
