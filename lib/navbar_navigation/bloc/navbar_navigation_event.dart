part of 'navbar_navigation_bloc.dart';

abstract class NavbarNavigationEvent extends Equatable {
  const NavbarNavigationEvent();

  @override
  List<Object> get props => [];
}

class NavbarIndexChanged extends NavbarNavigationEvent {
  final int index;
  const NavbarIndexChanged(this.index);

  @override
  List<Object> get props => [index];
}
