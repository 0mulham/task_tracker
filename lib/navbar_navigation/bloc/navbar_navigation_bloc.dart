import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../data/navbar_items.dart';

part 'navbar_navigation_event.dart';
part 'navbar_navigation_state.dart';

class NavbarNavigationBloc
    extends Bloc<NavbarNavigationEvent, NavbarNavigationState> {
  NavbarNavigationBloc()
      : super(const NavbarNavigationInitial(NavbarItem.home, 0)) {
    on<NavbarIndexChanged>((event, emit) {
      var currentItem = NavbarItem.home;
      switch (event.index) {
        case 0:
          currentItem = NavbarItem.home;
          break;
        case 1:
          currentItem = NavbarItem.settings;
          break;
        default:
          currentItem = NavbarItem.home;
          break;
      }
      emit(NavbarNavigationCurrent(currentItem, event.index));
    });
  }
}
