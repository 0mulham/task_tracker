part of 'navbar_navigation_bloc.dart';

abstract class NavbarNavigationState extends Equatable {
  final NavbarItem navbarItem;
  final int index;

  const NavbarNavigationState(this.navbarItem, this.index);

  @override
  List<Object> get props => [navbarItem, index];
}

class NavbarNavigationInitial extends NavbarNavigationState {
  const NavbarNavigationInitial(super.navbarItem, super.index);

  @override
  List<Object> get props => [navbarItem, index];
}

class NavbarNavigationCurrent extends NavbarNavigationState {
  const NavbarNavigationCurrent(super.navbarItem, super.index);

  @override
  List<Object> get props => [navbarItem, index];
}
